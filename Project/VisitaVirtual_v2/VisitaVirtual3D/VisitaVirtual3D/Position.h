#pragma once
#include <stdlib.h>
/*OpenGL includes*/
#include <GL/glut.h>
#include <GL/glaux.h>

/*OpenAL includes*/
#include <AL/alut.h>
class Position
{
public:
	Position();
	void setPosition(GLfloat x, GLfloat y, GLfloat z);
	GLfloat getX();
	GLfloat getY();
	GLfloat getZ();

	void setX(GLfloat x);
	void setY(GLfloat y);
	void setZ(GLfloat z);

	~Position();
private:
	GLfloat    x, y, z;
};

