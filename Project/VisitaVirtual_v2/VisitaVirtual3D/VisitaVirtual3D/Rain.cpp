#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <GL/glut.h>R
#include <GL/gl.h>
#include "Rain.h"

using namespace std;

// Initialize/Reset Particles - give them their attributes
void Rain::initParticles(int i) {
	par_sys[i].alive =!rain_flag;
	par_sys[i].life = 100;
	par_sys[i].fade = 0; // float(rand() % 100) / 1000.0f + 0.003f;

	par_sys[i].xpos = (float)(rand() % 1000) -10;
	par_sys[i].ypos = 2;
	par_sys[i].zpos = (float)(rand() % 1000) -10;

	par_sys[i].red = 0.5;
	par_sys[i].green = 0.5;
	par_sys[i].blue = 1.0;

	par_sys[i].vel = (float)(rand() % 1000) - 10;
	par_sys[i].gravity = -0.1;//-0.8;

}

void Rain::init() {
	int x, z;

	//glShadeModel(GL_SMOOTH);
	//glClearColor(0.0, 0.0, 0.0, 0.0);
	//glClearDepth(1.0);
	glEnable(GL_DEPTH_TEST);

	// Initialize particles
	for (loop = 0; loop < MAX_PARTICLES; loop++) {
		initParticles(loop);
		
	}
}

// For Rain
void Rain::drawRain() {
	float x, y, z;
	for (loop = 0; loop < MAX_PARTICLES; loop = loop + 2) {
		if (par_sys[loop].alive == true) {
			x = par_sys[loop].xpos ;
			y = par_sys[loop].ypos ;
			z = par_sys[loop].zpos + zoom ;

			// Draw particles
			glColor3f(0.5, 0.5, 1.0);
			glBegin(GL_LINES);
			glVertex3f(x, y, z);
			glVertex3f(x, y + 0.5, z);
			glEnd();

			// Update values
			//Move
			// Adjust slowdown for speed!
			par_sys[loop].ypos += par_sys[loop].vel / (slowdown * 1000);
			par_sys[loop].vel += par_sys[loop].gravity;
			// Decay
			//par_sys[loop].life -= par_sys[loop].fade;

		//	if (par_sys[loop].ypos <= -10) {
		//		par_sys[loop].life = -1.0;
			//}
			//Revive
		//	if (par_sys[loop].life < 0.0) {
		//		initParticles(loop);
			//}
		}
	}
}
// Draw Particles
void Rain::drawScene() {
	int i, j;
	float x, y, z;

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();


	//glRotatef(pan, 0.0, 1.0, 0.0);
	//glRotatef(tilt, 1.0, 0.0, 0.0);


	glEnd();
	// Which Particles
	if (fall == RAIN) {
		drawRain();
	}


	glutSwapBuffers();
}

void Rain::reshape(int w, int h) {
	if (h == 0) h = 1;

	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45, (float)w / (float)h, .1, 200);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Rain::idle() {
	glutPostRedisplay();
}
