#pragma once
#include "Keys.h"
#include "Camera.h"



class State
{
public:
	
	Camera      camera;
	GLint         timer;
	Keys      teclas;
	GLuint        vista[NUM_JANELAS];
	ALuint        buffer[9], source[3];

	State();

	void setMainWindow(GLint mainWindow);
	void setTopWindow(GLint topSubwindow);
	void setNavigateWindow(GLint navigate);
	void setLocalViewer(GLboolean localViewer);

	GLint getMainWindow();
	GLint getTopWindow();
	GLint getNavigateWindow();
	GLboolean getLocalViewer();


	~State();
private:
	GLint         mainWindow, topSubwindow, navigateSubwindow;
	GLboolean     localViewer;

};

