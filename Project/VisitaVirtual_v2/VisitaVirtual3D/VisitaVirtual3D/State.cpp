#include "State.h"




State::State()
{
	

}



void State::setMainWindow(GLint mainWindow)
{
	this->mainWindow = mainWindow;
}

void State::setTopWindow(GLint topSubwindow)
{
	this->topSubwindow = topSubwindow;
}

void State::setNavigateWindow(GLint navigate)
{
	this->navigateSubwindow = navigate;
}

void State::setLocalViewer(GLboolean localViewer)
{
	this->localViewer = localViewer;
}



GLint State::getMainWindow()
{
	return this->mainWindow;
}

GLint State::getTopWindow()
{
	return this->topSubwindow;
}

GLint State::getNavigateWindow()
{
	return this->navigateSubwindow;
}

GLboolean State::getLocalViewer()
{
	return this->localViewer;
}


State::~State()
{
}
