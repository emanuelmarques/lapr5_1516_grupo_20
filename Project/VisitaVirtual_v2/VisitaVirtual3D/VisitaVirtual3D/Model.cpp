#include "Model.h"



Model::Model():objecto()
{
}


Model::~Model()
{
}

void Model::setXMouse(GLuint x)
{
	this->xMouse = x;
}

void Model::setYMouse(GLuint y)
{
	this->yMouse = y;
}

void Model::setAndar(GLboolean andar)
{
	this->andar = andar;
}
void Model::setDescer(GLboolean descer)
{
	this->Descer = descer;
}
void Model::setSubir(GLboolean subir)
{
	this->Subir = subir;
}

void Model::setPrev(GLuint prev)
{
	this->prev = prev;
}

GLuint Model::getXMouse()
{
	return this->xMouse;
}

GLuint Model::getYMouse()
{
	return this->yMouse;
}

GLboolean Model::getAndar()
{
	return this->andar;
}

GLboolean Model::getDescer()
{
	return this->Descer;
}
GLboolean Model::getSubir()
{
	return this->Subir;
}
GLuint Model::getPrev()
{
	return this->prev;
}

GLboolean detectaColisao(GLfloat nx, GLfloat nz, char mazedata[NUM_PISOS+1][MAZE_HEIGHT][MAZE_WIDTH + 1], State &estado, Model &modelo)
{
	GLuint i = (nz + MAZE_HEIGHT*0.5 + 0.25), j = (int)(nx + MAZE_WIDTH*0.5 + 0.25);
	if ( (mazedata[modelo.pisoActual][i][j] == '-' || mazedata[modelo.pisoActual][i][j] == 'p' || mazedata[modelo.pisoActual][i][j] == 'P' && mazedata[modelo.pisoActual][i][j] != ' ' && mazedata[modelo.pisoActual][i][j] != 'E' && mazedata[modelo.pisoActual][i][j] != 'D'  && mazedata[modelo.pisoActual][i][j] != '<'  && mazedata[modelo.pisoActual][i][j] != '>') ||
		 (mazedata[modelo.pisoActual][i][j] == '|' || mazedata[modelo.pisoActual][i][j] == 'p' || mazedata[modelo.pisoActual][i][j] == 'P' && mazedata[modelo.pisoActual][i][j] != ' ' && mazedata[modelo.pisoActual][i][j] != 'E' && mazedata[modelo.pisoActual][i][j] != 'D'  && mazedata[modelo.pisoActual][i][j] != '<'  && mazedata[modelo.pisoActual][i][j] != '>') ||
		 (mazedata[modelo.pisoActual][i][j] == '+' || mazedata[modelo.pisoActual][i][j] == 'p' || mazedata[modelo.pisoActual][i][j] == 'P' && mazedata[modelo.pisoActual][i][j] != ' ' && mazedata[modelo.pisoActual][i][j] != 'E' && mazedata[modelo.pisoActual][i][j] != 'D'  && mazedata[modelo.pisoActual][i][j] != '<'  && mazedata[modelo.pisoActual][i][j] != '>'))
	{
		if (modelo.homer[JANELA_NAVIGATE].GetSequence() != 20)
		{
			modelo.homer[JANELA_TOP].SetSequence(20);
			modelo.homer[JANELA_NAVIGATE].SetSequence(20);
		}
		alSourceStop(estado.source[1]);
		alSourcei(estado.source[1], AL_BUFFER, estado.buffer[rand() % 3 + 6]);
		alSourcePlay(estado.source[1]);
		modelo.posicaoAnterior = mazedata[modelo.pisoActual][i][j];
		return(GL_TRUE);
	}
	else if (mazedata[modelo.pisoActual][i][j] == '>' && modelo.posicaoAnterior == 'E')
	{
		modelo.setSubir(GL_TRUE);
	}
	else if (mazedata[modelo.pisoActual][i][j] == '<' && modelo.posicaoAnterior == 'D') {
		modelo.setDescer(GL_TRUE);
	}
	modelo.posicaoAnterior = mazedata[modelo.pisoActual][i][j];
	return(GL_FALSE);
}