#include "Object.h"



Object::Object():pos()
{
	
}


Object::~Object()
{
}


GLfloat Object::getDir() {
	return this->dir;
}
GLfloat Object::getVel() {
	return this->vel;
}

void Object::setPos(Position pos) {
	this->pos = pos;

}
void Object::setDir(GLfloat dir) {
	this->dir = dir;
}
void Object::setVel(GLfloat vel) {
	this->vel = vel;
}

void Object::incDir(GLfloat dir)
{
	this->dir += dir;
}
