#pragma once
#ifndef SKYBOX_H
#define SKYBOX_H

#include <stdio.h>
#include <stdlib.h>
#include <GL\glut.h>
#include <GL\GLAux.h>
#include <GL\GL.h>
#include <GL\GLU.h>

#define NUM_TEXTURAS         6

void drawSkybox(float x, float y, float z, float width, float height, float length);
void LoadTexture();


#endif
