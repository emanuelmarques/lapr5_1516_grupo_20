#pragma once
#include "mdlviewer.h"
#include "Configs.h"
#include "Object.h"
#include "State.h"


class Model
{
public:
	
	GLuint        texID[NUM_JANELAS][NUM_TEXTURAS];
	GLuint        imovel[NUM_JANELAS];
	GLuint		  posEscadas[2];
	GLuint        chao[NUM_JANELAS];
	GLuint		  teto[NUM_JANELAS];
	GLboolean 	  dia;
	GLuint		  pisoActual;
	char 		  posicaoAnterior;

	Object	    objecto;
	StudioModel   homer[NUM_JANELAS];   // Modelo da Personagem
	Model();
	

	void setXMouse(GLuint x);
	void setYMouse(GLuint y);
	void setAndar(GLboolean andar);
	void setPrev(GLuint prev);
	void setSubir(GLboolean subir);
	void setDescer(GLboolean descer);

	GLuint        getXMouse();
	GLuint        getYMouse();
	GLboolean     getAndar();
	GLuint        getPrev();
	GLboolean	  getSubir();
	GLboolean	  getDescer();

	~Model();
private:
	GLuint        xMouse;
	GLuint        yMouse;
	GLboolean     andar;
	GLboolean	  Descer;
	GLboolean     Subir;
	GLuint        prev;
};
GLboolean detectaColisao(GLfloat nx, GLfloat nz, char mazedata[NUM_PISOS+1][MAZE_HEIGHT][MAZE_WIDTH + 1], State &estado, Model &modelo);
