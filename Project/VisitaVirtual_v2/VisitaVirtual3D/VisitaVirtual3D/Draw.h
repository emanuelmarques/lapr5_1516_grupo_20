#include "Main.h"
#include "glm.h"


void desenhaAngVisao(Camera *cam);
void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat xa, GLfloat ya, GLfloat xb, GLfloat yb, GLfloat xc, GLfloat yc, GLfloat xd, GLfloat yd);
void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat tx, GLfloat ty);
void desenhaBussola(int width, int height, State &estado);
void desenhaCubo(int tipo, GLuint texID);
void desenhaModeloDir(Object obj, int width, int height);
void desenhaModelo(State &estado, Model &modelo);
void desenhaImovel(GLuint texID, char mazedata[NUM_PISOS+1][MAZE_HEIGHT][MAZE_WIDTH + 1]);
void desenhaChao(GLfloat dimensao, GLuint texID, GLuint piso, Model &modelo);
void desenhaTeto(GLfloat dimensao, GLuint texID, GLuint piso, Model &modelo);
void importPlant(char * path, GLuint piso, Model &modelo, char mazedata[NUM_PISOS + 1][MAZE_HEIGHT][MAZE_WIDTH + 1]);