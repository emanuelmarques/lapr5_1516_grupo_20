#pragma once
#include <stdlib.h>
#include <GL/glut.h>

#define	OBJECTO_ALTURA			0.4
#define OBJECTO_VELOCIDADE		0.5
#define OBJECTO_ROTACAO			5
#define OBJECTO_RAIO		    0.12
#define SCALE_HOMER				0.005
#define EYE_ROTACAO			    1

#define MAZE_HEIGHT				18
#define MAZE_WIDTH				18
#define	CHAO_DIMENSAO			18
#define	TETO_DIMENSAO			18

#define NUM_JANELAS				2
#define JANELA_TOP              0
#define JANELA_NAVIGATE         1

#define NOME_TEXTURA_CUBOS        "Textura.bmp"
#define ID_TEXTURA_CUBOS          0
#define NOME_TEXTURA_PAREDES	  "parede.bmp"
#define ID_TEXTURA_PAREDES		  1
#define NOME_TEXTURA_CHAO         "Chao.jpg"
#define ID_TEXTURA_CHAO           2
#define NOME_TEXTURA_TETO		  "Teto.bmp"
#define ID_TEXTURA_TETO			  3
#define NOME_TEXTURA_CEU		  "Ceu.jpg"
#define ID_TEXTURA_CEU		      4
#define NOME_TEXTURA_DEGRAU		  "degrau.bmp"
#define ID_TEXTURA_DEGRAU	      5
#define NOME_TEXTURA_NOITE		  "Ceu.jpg"
#define ID_TEXTURA_NOITE	      6

#define NUM_TEXTURAS              7
#define NUM_PISOS				  2 //alterar para dinamico

#define	GAP					25


