#pragma once
#include "SkyBox.h"

char *texts[] = { "front.jpg" , "back.jpg", "left.jpg", "right.jpg", "up.jpg", "down.jpg" };
GLuint texturs[NUM_TEXTURAS];

extern "C" int read_JPEG_file(char *, char **, int *, int *, int *);

void drawSkybox(float x, float y, float z, float width, float height, float length)
{
	LoadTexture();
	//glPushMatrix();
	// Center the Skybox around the given x,y,z position
	x = x - width / 2;
	y = y - height / 2;
	z = z - length / 2;
	// Draw Front 
	glBindTexture(GL_TEXTURE_2D, texturs[0]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(x, y, z + length);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(x, y + height, z + length);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x + width, y, z + length);
	glEnd();
	// Draw Back 
	glBindTexture(GL_TEXTURE_2D, texturs[1]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(x + width, y, z);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(x + width, y + height, z);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(x, y + height, z);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x, y, z);
	glEnd();
	// Draw Left
	glBindTexture(GL_TEXTURE_2D, texturs[2]);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(x, y + height, z);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(x, y + height, z + length);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x, y, z + length);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(x, y, z);
	glEnd();
	// Draw Right 
	glBindTexture(GL_TEXTURE_2D, texturs[3]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x + width, y, z);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(x + width, y, z + length);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(x + width, y + height, z);
	glEnd();
	// Draw Up 
	glBindTexture(GL_TEXTURE_2D, texturs[4]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x + width, y + height, z);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x + width, y + height, z + length);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(x, y + height, z + length);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(x, y + height, z);
	glEnd();
	// Draw Down
	glBindTexture(GL_TEXTURE_2D, texturs[5]);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 1.0f); glVertex3f(x, y, z);
	glTexCoord2f(1.0f, 1.0f); glVertex3f(x, y, z + length);
	glTexCoord2f(1.0f, 0.0f); glVertex3f(x + width, y, z + length);
	glTexCoord2f(0.0f, 0.0f); glVertex3f(x + width, y, z);
	glEnd();


}

void LoadTexture()
{

	char *image;
	int w, h, bpp;

#ifdef _WIN32
	AUX_RGBImageRec *TextureImage[8];

	memset(TextureImage, 0, sizeof(void *) * 8);
#endif

	glGenTextures(NUM_TEXTURAS, texturs);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	for (size_t i = 0; i < 6; i++)
	{
		if (read_JPEG_file(texts[i], &image, &w, &h, &bpp))
		{
			glBindTexture(GL_TEXTURE_2D, texturs[i]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			gluBuild2DMipmaps(GL_TEXTURE_2D, 3, w, h, GL_RGB, GL_UNSIGNED_BYTE, image);
		}
		else {
			printf("Textura %s not Found\n", texts[i]);
			exit(0);
		}
		glBindTexture(GL_TEXTURE_2D, NULL);
	}
}