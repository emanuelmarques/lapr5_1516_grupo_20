﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cliente_IT3.Startup))]
namespace Cliente_IT3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
