﻿using System;

namespace Cliente_It3.ViewModels
{
    public class VisitVM
    {
        public int ID { get; set; }

        public DateTime Day { get; set; }

        public DateTime StartHour { get; set; }

        public DateTime EndHour { get; set; }

        public string User { get; set; }
        
        public int realtyID { get; set; }
        public string realty { get; set; }

        public VisitVM() { }

  
    }
}
