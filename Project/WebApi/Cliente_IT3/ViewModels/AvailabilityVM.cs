﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cliente_IT3.ViewModels
{
    public class AvailabilityVM
    {
        public int ID { get; set; }

        public DateTime Day { get; set; }

        public DateTime StartHour { get; set; }

        public DateTime EndHour { get; set; }

        public string User { get; set; }

        public int realtyID { get; set; }

        public string realty { get; set; }

        public Boolean isProprietary(string u)
        {
            if (u.Equals(this.User))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}