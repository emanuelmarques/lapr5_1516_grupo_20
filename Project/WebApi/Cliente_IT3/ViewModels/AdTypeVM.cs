﻿namespace Cliente_IT3.ViewModels
{
    public class AdTypeVM
    {
        public int AdTypeID { get; set; }
        public string Description { get; set; }
    }
}
