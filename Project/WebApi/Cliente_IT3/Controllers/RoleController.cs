﻿using Cliente_IT3.Helpers;
using Cliente_IT3.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Cliente_IT3.ViewModels;

namespace Cliente_IT3.Controllers
{
    public class RoleController : Controller
    {
        // GET: Role
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var roles =
                JsonConvert.DeserializeObject<IEnumerable<RoleVM>>(content);

                return View(roles);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Role/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Role/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RoleVM rvm)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string roleJSON = JsonConvert.SerializeObject(rvm);
                HttpContent content = new StringContent(roleJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Role", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Role/Delete/5
        public async Task<ActionResult> Delete(String id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DeleteConfirmed(id);
            return RedirectToAction("Index");
        }

        // POST: Role/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(String id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Role?name=" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        public async Task<ActionResult> addRoleToUser()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Role");
            var roles = new List<RoleVM>();
            var users = new List<ApplicationUser>();
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                roles =
                JsonConvert.DeserializeObject<IEnumerable<RoleVM>>(content).ToList();

                HttpResponseMessage response2 = await client.GetAsync("api/User");
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    users =
                    JsonConvert.DeserializeObject<IEnumerable<ApplicationUser>>(content2).ToList();
                }
                else
                {
                    return Content("Ocorreu um erro: " + response2.StatusCode);
                }

            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            List<String> users2 = (from u in users select u.UserName).ToList();
            List<String> roles2 = (from r in roles select r.name).ToList();

            ViewBag.Roles = new SelectList(roles2);
            ViewBag.Users = new SelectList(users2);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> addRoleToUser(RoleVM role)
        {
            var client = WebApiHttpClient.GetClient();
            string roleJSON = JsonConvert.SerializeObject(role);
            HttpContent content = new StringContent(roleJSON,
            System.Text.Encoding.Unicode, "application/json");
            var response = await client.PutAsync("api/addRoleToUser",content);
            if (response.IsSuccessStatusCode)
            {
                HttpResponseMessage response2 = await client.GetAsync("api/Role");
                var roles2 = new List<RoleVM>();
                var users2 = new List<ApplicationUser>();
                if (response2.IsSuccessStatusCode)
                {

                    if (role.user.Equals(WebApiHttpClient.userLogged.UserName))
                    {
                        WebApiHttpClient.role.Add(role.name);
                    }

                    string content2 = await response2.Content.ReadAsStringAsync();
                    roles2 =
                    JsonConvert.DeserializeObject<IEnumerable<RoleVM>>(content2).ToList();

                    HttpResponseMessage response3 = await client.GetAsync("api/User");
                    if (response3.IsSuccessStatusCode)
                    {
                        string content3 = await response3.Content.ReadAsStringAsync();
                        users2 =
                        JsonConvert.DeserializeObject<IEnumerable<ApplicationUser>>(content3).ToList();
                    }
                    else
                    {
                        return Content("Ocorreu um erro: " + response2.StatusCode);
                    }

                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }

                List<String> users3 = (from u in users2 select u.UserName).ToList();
                List<String> roles3 = (from r in roles2 select r.name).ToList();

                ViewBag.Roles = new SelectList(roles3);
                ViewBag.Users = new SelectList(users3);

                return View("AddRoleToUser");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteRoleForUser(string userName, string roleName)
        {
            var client = WebApiHttpClient.GetClient();
            var response = await client.DeleteAsync("api/Role?userName=" + userName + "&roleName=" + roleName);

            if (response.IsSuccessStatusCode)
            {

                if (userName.Equals(WebApiHttpClient.userLogged.UserName))
                {
                    WebApiHttpClient.role.Remove(roleName);
                }

                HttpResponseMessage response2 = await client.GetAsync("api/Role");
                var roles2 = new List<RoleVM>();
                var users2 = new List<ApplicationUser>();
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    roles2 =
                    JsonConvert.DeserializeObject<IEnumerable<RoleVM>>(content2).ToList();

                    HttpResponseMessage response3 = await client.GetAsync("api/User");
                    if (response3.IsSuccessStatusCode)
                    {
                        string content3 = await response3.Content.ReadAsStringAsync();
                        users2 =
                        JsonConvert.DeserializeObject<IEnumerable<ApplicationUser>>(content3).ToList();
                    }
                    else
                    {
                        return Content("Ocorreu um erro: " + response2.StatusCode);
                    }

                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }

                List<String> users3 = (from u in users2 select u.UserName).ToList();
                List<String> roles3 = (from r in roles2 select r.name).ToList();

                ViewBag.Roles = new SelectList(roles3);
                ViewBag.Users = new SelectList(users3);

                return View("AddRoleToUser");
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> GetUserRoles(string userName)
        {
            var client = WebApiHttpClient.GetClient();
            var response = await client.GetAsync("api/Role?userName=" + userName);

            List<string> roles = new List<string>();

            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var role =
                JsonConvert.DeserializeObject<IEnumerable<RoleVM>>(content);


                for (var i=0; i< role.Count(); i++)
                {
                    roles.Add(role.ElementAt(i).name);
                }

                if (userName.Equals(WebApiHttpClient.userLogged.UserName))
                {
                    WebApiHttpClient.addRole(roles);
                }
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
            ViewBag.RolesForThisUser = roles;

            HttpResponseMessage response2 = await client.GetAsync("api/Role");
            var roles2 = new List<RoleVM>();
            var users2 = new List<ApplicationUser>();
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                roles2 =
                JsonConvert.DeserializeObject<IEnumerable<RoleVM>>(content2).ToList();

                HttpResponseMessage response3 = await client.GetAsync("api/User");
                if (response3.IsSuccessStatusCode)
                {
                    string content3 = await response3.Content.ReadAsStringAsync();
                    users2 =
                    JsonConvert.DeserializeObject<IEnumerable<ApplicationUser>>(content3).ToList();
                }
                else
                {
                    return Content("Ocorreu um erro: " + response2.StatusCode);
                }

            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }

            List<String> users3 = (from u in users2 select u.UserName).ToList();
            List<String> roles3 = (from r in roles2 select r.name).ToList();

            ViewBag.Roles = new SelectList(roles3);
            ViewBag.Users = new SelectList(users3);

            return View("AddRoleToUser");
        }
    }
}