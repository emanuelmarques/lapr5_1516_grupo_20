﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cliente_IT3.Models;
using Cliente_It3.ViewModels;
using System.Threading.Tasks;
using Cliente_IT3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Cliente_IT3.Controllers
{
    public class VisitController : Controller
    {

        // GET: Availibility
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Visit");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var availability =
                JsonConvert.DeserializeObject<IEnumerable<VisitVM>>(content);
                return View(availability);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }
    }
}
