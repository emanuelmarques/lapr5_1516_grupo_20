﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cliente_IT3.ViewModels;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Cliente_IT3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Cliente_IT3.Controllers
{
    public class RealtyTypeController : Controller
    {
        // GET: RealtyType
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/RealtyType");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rts =
                JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content);
                return View(rts);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: RealtyType/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/RealtyType/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rt = JsonConvert.DeserializeObject<RealtyTypeVM>(content);
                if (rt == null) return HttpNotFound();
                return View(rt);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }


        // GET: RealtyType/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/RealtyType");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rts =
                JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content);

                ViewBag.MainTypeID = new SelectList(rts, "RealtyTypeID", "Description");
            }

            return View();
        }

        // POST: RealtyType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RealtyTypeVM rt)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string rtJSON = JsonConvert.SerializeObject(rt);
                HttpContent content = new StringContent(rtJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/RealtyType", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: RealtyType/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/RealtyType/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rt = JsonConvert.DeserializeObject<RealtyTypeVM>(content);
                if (rt == null) return HttpNotFound();

                HttpResponseMessage response2 = await client.GetAsync("api/RealtyType");
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    var rts =
                    JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content2);

                    var listRT = rts.ToList();
                    for (var i=0; i< listRT.Count; i++)
                    {
                        if (listRT[i].Description.Equals(rt.Description))
                        {
                            listRT.Remove(listRT[i]);
                        }
                    }

                    rts = (IEnumerable<RealtyTypeVM>)listRT;

                    ViewBag.MainTypeID = new SelectList(rts, "RealtyTypeID", "Description", rt.MainTypeID);
                }

                return View(rt);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: RealtyType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "RealtyTypeID, MainTypeID, Description, MainTypeDescription")] RealtyTypeVM rt)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string rtJSON = JsonConvert.SerializeObject(rt);
                HttpContent content = new StringContent(rtJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/RealtyType/" + rt.RealtyTypeID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }


        // GET: RealtyType/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/RealtyType/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var rt = JsonConvert.DeserializeObject<RealtyTypeVM>(content);
                if (rt == null) return HttpNotFound();
                return View(rt);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: RealtyType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/RealtyType/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["erro2"] = "Impossible to delete. There are Ads of this Realty Type.";

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

    }
}
