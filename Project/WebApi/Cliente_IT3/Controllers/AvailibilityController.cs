﻿using Cliente_IT3.Helpers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Cliente_IT3.ViewModels;
using System.Net;

namespace Cliente_IT3.Controllers
{
    public class AvailibilityController : Controller
    {
        // GET: Availibility
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Availability");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var availability =
                JsonConvert.DeserializeObject<IEnumerable<AvailabilityVM>>(content);
                return View(availability);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Availibility/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Availability/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var availibility = JsonConvert.DeserializeObject<AvailabilityVM>(content);
                if (availibility == null) return HttpNotFound();
                return View(availibility);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: Availibility/Create
        public async Task<ActionResult> Create(int? id)
        {
            var client = WebApiHttpClient.GetClient();
           var availibility = new AvailabilityVM();
            HttpResponseMessage response2 = await client.GetAsync("api/ad/"+id);
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
               var ad = JsonConvert.DeserializeObject<AdVM>(content2);
                availibility.realtyID = ad.RealtyID;
                availibility.User = WebApiHttpClient.userLogged.UserName;

            }



            return View(availibility);
        }

        // POST: Availibility/Create
        [HttpPost]
        public async Task<ActionResult> Create(AvailabilityVM Availability)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string adJSON = JsonConvert.SerializeObject(Availability);
                HttpContent content = new StringContent(adJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Availability", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("index", "AdViewModel", new { area = "" });
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Availibility/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Availability/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var availability = JsonConvert.DeserializeObject<AvailabilityVM>(content);
                if (availability == null) return HttpNotFound();
                
                return View(availability);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Availibility/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AvailabilityVM avai)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string adJSON = JsonConvert.SerializeObject(avai);
                HttpContent content = new StringContent(adJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Availability/" + avai.ID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: Availibility/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Availability/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var avai = JsonConvert.DeserializeObject<AvailabilityVM>(content);
                if (avai == null) return HttpNotFound();
                return View(avai);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: Availibility/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Availability/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }
    }
}
