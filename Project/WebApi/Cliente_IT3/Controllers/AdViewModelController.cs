﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cliente_IT3.ViewModels;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using Cliente_IT3.Helpers;
using System.Net.Http;
using Newtonsoft.Json;

namespace Cliente_IT3.Controllers
{
    public class AdViewModelController : Controller
    {

        // GET: AdViewModel
        public async Task<ActionResult> Index()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Ad");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var ads =
                JsonConvert.DeserializeObject<IEnumerable<AdVM>>(content);
                return View(ads);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }

        // GET: AdViewModel/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Ad/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var ad = JsonConvert.DeserializeObject<AdVM>(content);
                if (ad == null) return HttpNotFound();
                return View(ad);
            }
            else
            {
                return Content("Ocorreu um erro: " + response.StatusCode);
            }
        }


        // GET: AdViewModel/Create
        public async Task<ActionResult> Create()
        {
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/AdType");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var adts =
                JsonConvert.DeserializeObject<IEnumerable<AdTypeVM>>(content);

                ViewBag.AdTypeID = new SelectList(adts, "AdTypeID", "Description");
            }

            var rts = new List<RealtyTypeVM>();

            HttpResponseMessage response2 = await client.GetAsync("api/RealtyType");
            if (response2.IsSuccessStatusCode)
            {
                string content2 = await response2.Content.ReadAsStringAsync();
                rts =
                JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content2).ToList();

                ViewBag.RealtyTypeID = new SelectList(rts, "RealtyTypeID", "Description");
            }

            if (rts.Count == 0)
            {
                TempData["erro"] = "Impossible to create Ad. No Realty Types created.";
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        // POST: AdViewModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(AdVM ad)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string adJSON = JsonConvert.SerializeObject(ad);
                HttpContent content = new StringContent(adJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response = await client.PostAsync("api/Ad", content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

        // GET: AdViewModel/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Ad/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var ad = JsonConvert.DeserializeObject<AdVM>(content);
                if (ad == null) return HttpNotFound();

                HttpResponseMessage response2 = await client.GetAsync("api/AdType");
                if (response2.IsSuccessStatusCode)
                {
                    string content2 = await response2.Content.ReadAsStringAsync();
                    var adts =
                    JsonConvert.DeserializeObject<IEnumerable<AdTypeVM>>(content2);

                    ViewBag.AdTypeID = new SelectList(adts, "AdTypeID", "Description");
                }

                HttpResponseMessage response3 = await client.GetAsync("api/RealtyType");
                if (response3.IsSuccessStatusCode)
                {
                    string content3 = await response3.Content.ReadAsStringAsync();
                    var rts =
                    JsonConvert.DeserializeObject<IEnumerable<RealtyTypeVM>>(content3);

                    ViewBag.RealtyTypeID = new SelectList(rts, "RealtyTypeID", "Description");
                }

                return View(ad);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: AdViewModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(AdVM ad)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                string adJSON = JsonConvert.SerializeObject(ad);
                HttpContent content = new StringContent(adJSON,
                System.Text.Encoding.Unicode, "application/json");
                var response =
                await client.PutAsync("api/Ad/" + ad.AdID, content);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }


        // GET: AdViewModel/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var client = WebApiHttpClient.GetClient();
            HttpResponseMessage response = await client.GetAsync("api/Ad/" + id);
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var ad = JsonConvert.DeserializeObject<AdVM>(content);
                if (ad == null) return HttpNotFound();
                return View(ad);
            }
            return Content("Ocorreu um erro: " + response.StatusCode);
        }

        // POST: AdViewModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                var client = WebApiHttpClient.GetClient();
                var response = await client.DeleteAsync("api/Ad/" + id);
                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return Content("Ocorreu um erro: " + response.StatusCode);
                }
            }
            catch
            {
                return Content("Ocorreu um erro.");
            }
        }

    }
}
