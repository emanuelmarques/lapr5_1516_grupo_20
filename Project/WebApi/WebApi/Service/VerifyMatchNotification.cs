﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RealEstateLibrary.Models;
using WebApi.Repository;
using WebApi.ViewModels;
using Web_API.Repository;

namespace WebApi.Service
{
    public class VerifyMatchNotification : IObserver<Alert>, IObserver<Ad>
    {

        private AdRepository Ad_repo = new AdRepository();
        private AlertRepository alert_repo = new AlertRepository();
        private NotificationRepository notification_repo = new NotificationRepository();
        private Notification notification = new Notification();
        private IDisposable cancellation;

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }


        public void OnNext(Ad value)
        {
            foreach (var alert in alert_repo.GetAllAlerts())
                

                if (alert.Address == value.Realty.Address || alert.AdType == value.AdType || alert.MaxArea <= value.Realty.Area ||  alert.MaxCost  <= value.Cost)
                {
                    notification.userName = alert.UserName;
                    notification.menssage = "Tem uma notificação para o seu alerta " + alert.AlertID + " correspondente ao anúncio: " + value.AdID;
                    notification.notified = false;
                    notification_repo.InsertNotification(notification);
                    notification_repo._ctx.SaveChanges();
                }
        }

        public void OnNext(Alert value)
        {
            foreach (var ad in Ad_repo.GetAllAds())

                if (value.Address == ad.Realty.Address || value.AdType == ad.AdType || value.MaxArea <= ad.Realty.Area || value.MaxCost <= ad.Cost)
                {

                    notification.userName = value.UserName;
                    notification.menssage = "Tem uma notificação para o seu alerta " + value.AlertID + " correspondente ao anúncio: " + ad.AdID;
                    notification.notified = false;
                    notification_repo.InsertNotification(notification);
                    notification_repo._ctx.SaveChanges();
                }
        }
    }
}