﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class RealtyRepository : ContextRepository
    {

        public RealtyRepository()
        {
        }

        public List<Realty> GetAllRealties()
        {
            return _ctx.Realty.ToList();
        }

        public Realty GetRealtyByID(int RealtyId) { return _ctx.Realty.Find(RealtyId); }

        public Realty InsertRealty(Realty realty) { return _ctx.Realty.Add(realty); }

        public bool addVisitToRealtyAgenda(Visit visit)
        {
            IEnumerable<Realty> realtyInThisVisit = this.GetAllRealties().Where(r => r.RealtyID == visit.RealtyID);
            if (realtyInThisVisit.Count() > 0)
            {
                Realty realty = realtyInThisVisit.ElementAt(0);
                if (realty.Schedule.Where(s => s.VisitID == visit.VisitID).Count() == 0)
                {
                    realty.Schedule.Add(visit);
                    return true;
                }
            }

            return false;
        }

        public string GetIDRealtiesMediador(string IDMediador)
        {

            string lista = "";
            // List<Realty> lista_imoveis = new List<Realty>();

            var ads = _ctx.Ad.Where(a => a.Mediator.Equals(IDMediador));
            //_ctx.Ad.
            foreach (Ad ad in ads)
            {
                int id = ad.RealtyID;
                var list = _ctx.Realty.ToList();
                var imovel = _ctx.Realty.Where(im => im.RealtyID == id).ToList().ElementAt(0);
                //lista_imoveis.Add(ad.Realty);
                //lista += ad.RealtyID + "(" + ad.Realty.Day.ToString() + "," + ad.Realty.StartHour.ToString() + "," + ad.Realty.EndHour.ToString() + ");";
                if (imovel != null)
                { 
                lista += "imovel(" + imovel.RealtyID
                    + ",disponibilidade(date(" + imovel.Day.Year + "-" + imovel.Day.Month + "-" + imovel.Day.Day
                    + "),time(" + imovel.StartHour.Hour + ":" + imovel.StartHour.Minute
                    + "),time(" + imovel.EndHour.Hour + ":" + imovel.EndHour.Minute
                    + ")))";
                }
                    
            }

            // remover o ultimo separador
            if (lista.Length > 0)
            {
                lista = lista.Remove(lista.LastIndexOf(";"));
            }

            return lista;
        }

        public void Update(Realty updatedRealty)
        {
            _ctx.Entry(updatedRealty).State = EntityState.Modified;

        }

        public void DeleteRealty(int id)
        {
            Realty rl = _ctx.Realty.Find(id);
            _ctx.Realty.Remove(rl);
        }

    }
}