﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RealEstateLibrary.Models;
using System.Data.Entity;
using WebApi.Repository;
using WebApi.Models;

namespace Web_API.Repository
{
    public class AdRepository : ContextRepository, IObservable<Ad> 
    {
        private List<IObserver<Ad>> _observers;
        private IDisposable cancellation;
        private Ad ad;

        public AdRepository()
        {
            _observers = new List<IObserver<Ad>>();
        }

        public List<Ad> GetAllAds()
        {
            var list = _ctx.Ad.ToList();

            return list;
        }

        public Ad GetAdByID(int AdId) { return _ctx.Ad.Find(AdId); }

        public Ad InsertAd(Ad Ad)
        {
            this.ad = Ad;
            return _ctx.Ad.Add(Ad);
        }


        public List<Ad> GetAdByRealtyType(int RealtyTypeId)
        {
            return _ctx.Ad.Where(a => a.Realty.RealtyTypeID.Equals(RealtyTypeId)).ToList();
        }

        public List<Ad> GetAdByAdType(int AdTypeId)
        {
            return _ctx.Ad.Where(a => a.AdTypeID.Equals(AdTypeId)).ToList();
        }

        public void Update(Ad updatedAd)
        {
            _ctx.Entry(updatedAd).State = EntityState.Modified;

        }

        public void DeleteAd(int id)
        {
            Ad ad = _ctx.Ad.Find(id);
            _ctx.Ad.Remove(ad);
        }



        //public virtual void Subscribe(AlertRepository provider)
        //{
        //    cancellation = provider.Subscribe(this);
        //}
        public IDisposable Subscribe(IObserver<Ad> observer)
        {
            if (!_observers.Contains(observer))
            {
                _observers.Add(observer); 
            }
            //foreach (var item in GetAllAds())
                observer.OnNext(this.ad);
            return new Unsubscriber<Ad>(_observers, observer);
        }
    }




    class Unsubscriber<Alert> : IDisposable
    {
        private List<IObserver<Alert>> _observers;
        private IObserver<Alert> _observer;

        internal Unsubscriber(List<IObserver<Alert>> observers, IObserver<Alert> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }
}