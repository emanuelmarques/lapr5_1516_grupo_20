﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class PhotoRepository : ContextRepository
    {

        public PhotoRepository()
        {
        }

        public List<Photo> GetAllPhotos()
        {
            return _ctx.Photo.ToList();
        }

        public Photo GetPhotoByID(int PhotoId) { return _ctx.Photo.Find(PhotoId); }

        public Photo InsertPhoto(Photo Photo) { return _ctx.Photo.Add(Photo); }

        public void Update(Photo updatedPhoto)
        {
            _ctx.Entry(updatedPhoto).State = EntityState.Modified;

        }

        public void DeletePhoto(int id)
        {
            Photo p = _ctx.Photo.Find(id);
            _ctx.Photo.Remove(p);
        }
    }
}