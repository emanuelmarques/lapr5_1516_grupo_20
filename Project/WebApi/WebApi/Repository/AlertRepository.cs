﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class AlertRepository : ContextRepository, IObservable<Alert>
    {
        private IDisposable cancellation;
        private List<IObserver<Alert>> observers;
        private Alert alert;

        public AlertRepository()
        {
            observers = new List<IObserver<Alert>>();
        }

        public List<Alert> GetAllAlerts()
        {
            return _ctx.Alert.ToList();
        }

        public Alert GetAlertByID(int AlertId) { return _ctx.Alert.Find(AlertId); }

        public Alert InsertAlert(Alert Alert)
        {
            this.alert = Alert;
            return _ctx.Alert.Add(Alert);
        }

        public void Update(Alert updatedAlert)
        {
            _ctx.Entry(updatedAlert).State = EntityState.Modified;

        }

        public void DeleteAlert(int id)
        {
            Alert al = _ctx.Alert.Find(id);
            _ctx.Alert.Remove(al);
        }

        public IDisposable Subscribe(IObserver<Alert> observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
            }
            //foreach (var item in GetAllAlerts())
                observer.OnNext(this.alert);
            return new Unsubscriber<Alert>(observers, observer);
        }


        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
    }

    class Unsubscriber<Ad> : IDisposable
    {
        private List<IObserver<Ad>> _observers;
        private IObserver<Ad> _observer;

        internal Unsubscriber(List<IObserver<Ad>> observers, IObserver<Ad> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }
}