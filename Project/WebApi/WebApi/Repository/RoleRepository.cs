﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Repository
{
    public class RoleRepository : ContextRepository
    {

        public RoleRepository()
        {
        }

        public List<IdentityRole> GetAllRoles()
        {
            var list = _ctx.Roles.ToList();

            return list;
        }

        public IdentityRole GetRoleByID(String RoleId) { return _ctx.Roles.Find(RoleId); }

        public IdentityRole InsertRole(IdentityRole role) { return _ctx.Roles.Add(role); }

        public void DeleteRole(IdentityRole role)
        {
            _ctx.Roles.Remove(role);
        }
    }
}