﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.ViewModels;

namespace WebApi.Repository
{
    public class VisitRepository : ContextRepository
    {
        public VisitRepository()
        {
        }

        public List<Visit> GetAllVisits()
        {
            return _ctx.Visits.ToList();
        }

        public List<VisitVM> GetAllVisitsVM()
        {
            var listVisits = this.GetAllVisits();
            var listVisitVMs = new List<VisitVM>();

            for (var i = 0; i < listVisits.Count; i++)
            {
                VisitVM visitVM = new VisitVM();
                listVisitVMs.Add(visitVM.setDados(listVisits[i]));
            }

            return listVisitVMs;
        }

        public Visit GetVisitByID(int VisitID) {
            return _ctx.Visits.Find(VisitID);
        }

        public  VisitVM GetVisitVMByID(int visitID)
        {
            VisitVM visitVM = new VisitVM();
            return visitVM.setDados(this.GetVisitByID(visitID));
        }

        public Visit InsertVisit(Visit visit) {
            return _ctx.Visits.Add(visit);
        }

        public void Update(Visit updatedVisit)
        {
            _ctx.Entry(updatedVisit).State = EntityState.Modified;

        }

        public void DeleteRealty(int id)
        {
            Realty rl = _ctx.Realty.Find(id);
            _ctx.Realty.Remove(rl);
        }
    }
}
