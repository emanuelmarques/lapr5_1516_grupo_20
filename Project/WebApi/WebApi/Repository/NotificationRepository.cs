﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.ViewModels;

namespace WebApi.Repository
{
    public class NotificationRepository : ContextRepository
    {

        public List<Notification> GetAllNotifications()
        {
            var list = _ctx.Notification.ToList();

            return list;
        }


        public List<NotificationVM> GetAllNotificationsVM()
        {
            var listNotification = _ctx.Notification.ToList(); ;
            var listNotificationVM = new List<NotificationVM>();

            for (var i = 0; i < listNotification.Count; i++)
            {
                NotificationVM notification = new NotificationVM();

                var notificationVM = notification.Translate(listNotification[i]);
                if(notificationVM.notified == false)
                listNotificationVM.Add(notificationVM);

            }
            return listNotificationVM;
        }


        public Notification GetNotificationByID(int NotificationId) { return _ctx.Notification.Find(NotificationId); }

        public NotificationVM GetNotificationVMByID(int NotificationId) {

            NotificationVM notificationVM = new NotificationVM();
            var notification =_ctx.Notification.Find(NotificationId);

            return notificationVM.Translate(notification);

        }


        public Notification InsertNotification(Notification notification) { return _ctx.Notification.Add(notification); }


        public bool Exists(int id)
        {
            return _ctx.Notification.Count(a => a.NotificationID == id) > 0;
        }
    }
}