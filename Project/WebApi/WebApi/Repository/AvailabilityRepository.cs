﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.ViewModels;

namespace WebApi.Repository
{
    public class AvailabilityRepository :ContextRepository
    {

       
        public AvailabilityRepository()
        {
        }

        public List<Availability> GetAllAvailabilities()
        {
            return _ctx.Availability.ToList();
        }

        public List<AvailabilityVM> GetAllAvailabilitiesVM()
        {
            var listAvailabilities = this.GetAllAvailabilities();
            var listAvailabilityVMs = new List<AvailabilityVM>();

            for (var i = 0; i < listAvailabilities.Count; i++)
            {
                AvailabilityVM availabilityVM = new AvailabilityVM();
                listAvailabilityVMs.Add(availabilityVM.setDados(listAvailabilities[i]));
            }

            return listAvailabilityVMs;
        }

        public Availability GetAvailabilityByID(int availabilityID)
        {
            return _ctx.Availability.Find(availabilityID);
        }

        public AvailabilityVM GetAvailabilityVMByID(int availabilityID)
        {
            AvailabilityVM availabilityVM = new AvailabilityVM();
            return availabilityVM.setDados(this.GetAvailabilityByID(availabilityID));
        }

        public Availability InsertAvailability(Availability availability) { return _ctx.Availability.Add(availability); }

        public void Update(Availability updatedAvailability)
        {
            _ctx.Entry(updatedAvailability).State = EntityState.Modified;

        }

        public void Delete(int id)
        {
            Availability a = _ctx.Availability.Find(id);
            _ctx.Availability.Remove(a);
        }
        /**
Metodo que retorna a lista de disponibilidades dado um userID
*/

        public List<AvailabilityVM> GetAvailabilityByUserID(string data, string userID)
        {
            DateTime data_recebida = parseData(data);
            var listAvailabilities = _ctx.Availability.Where(a => a.UserName.Equals(userID)&&DbFunctions.TruncateTime(a.Day) == data_recebida.Date).ToList();

            var listAvailabilityVMs = new List<AvailabilityVM>();


            for (var i = 0; i < listAvailabilities.Count; i++)
            {

                AvailabilityVM availabilityVM = new AvailabilityVM();

                listAvailabilityVMs.Add(availabilityVM.setDados(listAvailabilities[i]));

            }

            return listAvailabilityVMs;


        }
        public void DeleteRealty(int id)
        {
            Realty rl = _ctx.Realty.Find(id);
            _ctx.Realty.Remove(rl);
        }
        public string getClientesQueremVisitarImoveis(string data, string IDMediador)
        {
            DateTime data_recebida = parseData(data);
            string clientes = "";
            var listaDisp = this.GetAllAvailabilities().Where(u => u.Day.Date == data_recebida.Date);//.Where()
            List<string> clientesID = new List<string>();
            foreach (Availability a in listaDisp)
            {
                Ad ad = _ctx.Ad.Where(anuncio => anuncio.RealtyID == a.RealtyID).ToList().ElementAt(0);
                if (!clientesID.Contains(a.UserName)&& ad.Mediator.Equals(IDMediador))
                {
                    clientesID.Add(a.UserName);
                    clientes += a.UserName + ";";
                }
                
            }
            if(clientes.Length>0)
            {
                //clientes = clientes.Substring(0, clientes.Length - 1);
                clientes = clientes.Remove(clientes.LastIndexOf(";"));
            }
            return clientes;
        }
        public string getImoveisQueClienteQuerVisitar(string data, string IDMediador,string IDCliente)
        {
            string imoveis = "";

            DateTime data_recebida = parseData(data);
            //var user = _ctx.Users.Where(u => u.Id.Equals(IDCliente)).ToList().ElementAt(0);
            var listaDisp = this.GetAllAvailabilities().Where(u => u.UserName.Equals(IDCliente)&&u.Day.Date==data_recebida.Date);
            List<string> imoveisID = new List<string>();
            foreach (Availability a in listaDisp)
            {
                var ad = _ctx.Ad.Where(anuncio => anuncio.RealtyID == a.RealtyID).ToList().ElementAt(0);
         
                if (!imoveisID.Contains(a.RealtyID.ToString())&&ad.Mediator.Equals(IDMediador))
                {
                imoveisID.Add(a.RealtyID.ToString());
                imoveis += a.RealtyID + ";";
                }
            }

            if (imoveis.Length > 0)
            {
                //clientes = clientes.Substring(0, clientes.Length - 1);
                imoveis = imoveis.Remove(imoveis.LastIndexOf(";"));
            }
            return imoveis;
        }
        public List<AvailabilityVM> getDisponibilidadesImoveisClienteQuerVisitar(string data, string IDMediador,string IDCliente, int IDImovel)
        {
            DateTime data_recebida = parseData(data);
            var listAvailabilities = _ctx.Availability.Where(a => a.UserName.Equals(IDCliente)&&a.RealtyID==IDImovel&& DbFunctions.TruncateTime(a.Day)== data_recebida.Date).ToList();
            var listAvailabilityVMs = new List<AvailabilityVM>();


            for (var i = 0; i < listAvailabilities.Count; i++)
            {
                int id_imovel = listAvailabilities[i].RealtyID;
                Console.WriteLine("ok");
                var ad = _ctx.Ad.Where(anuncio => anuncio.RealtyID == id_imovel).ToList().ElementAt(0);
                if (ad.Mediator.Equals(IDMediador))
                { 
                AvailabilityVM availabilityVM = new AvailabilityVM();

                listAvailabilityVMs.Add(availabilityVM.setDados(listAvailabilities[i]));
                }
            }

            return listAvailabilityVMs;


        }
        DateTime parseData(string data)
        {
            DateTime dt =
                DateTime.ParseExact(data, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            return dt;
        }
    }
}