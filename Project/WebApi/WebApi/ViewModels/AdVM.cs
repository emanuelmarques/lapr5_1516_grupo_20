﻿using System;
using System.Collections.Generic;

namespace WebApi.ViewModels
{
    public class AdVM
    {
        public Boolean status { get; set; } = false;
        public int AdID { get; set; }
        public decimal Cost { get; set; }
        public string User { get; set; }
        public string Mediator { get; set; }
        public List<string> Photo { get; set; } = new List<string>();
        public List<int> PhotoID { get; set; } = new List<int>();

        public int AdTypeID { get; set; }
        public string AdTypeDescription { get; set; }

        public int RealtyID { get; set; }
        public string Address { get; set; }
        public decimal Area { get; set; }

        public int RealtyTypeID { get; set; }
        public string RealtyTypeDescription { get; set; }
        public int? MainTypeID { get; set; }

        public int GPSLocationID { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal Altitude { get; set; }

        public DateTime Day { get; set; }
        public DateTime StartHour { get; set; }
        public DateTime EndHour { get; set; }

        public Boolean isProprietary(string u)
        {
            if (u.Equals(this.User))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}