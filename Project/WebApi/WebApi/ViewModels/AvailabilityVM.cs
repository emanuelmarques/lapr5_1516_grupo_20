﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApi.Repository;

namespace WebApi.ViewModels

{
    public class AvailabilityVM
    {
        private RealtyRepository realty_repo = new RealtyRepository();

        public int ID { get; set; }

        public DateTime Day { get; set; }

        public DateTime StartHour { get; set; }

        public DateTime EndHour { get; set; }

        public string User { get; set; }

        public int realtyID { get; set; }

        public string realty { get; set; }

        public AvailabilityVM() { }

        public AvailabilityVM setDados(Availability v)
        {
            ID = v.AvaibilityID;
            Day = v.Day;
            StartHour = v.StartHour;
            EndHour = v.EndHour;
            User = v.UserName;
            realtyID = v.RealtyID;
            realty = v.Realty.RealtyType.Description;
            return this;
        }

        public Availability TranslateToAvailability(Availability v, AvailabilityVM availabilityVM)
        {
           
            v.Day = availabilityVM.Day;
            v.StartHour = availabilityVM.StartHour;
            v.EndHour = availabilityVM.EndHour;
            v.UserName = availabilityVM.User;
            v.RealtyID = availabilityVM.realtyID;
            //v.Realty = realty_repo.GetRealtyByID(v.RealtyID);
            //realty_repo._ctx.Dispose();


            return v;
        }

    }
}