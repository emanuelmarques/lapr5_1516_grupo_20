﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.ViewModels
{
    public class AlgavVisitVM
    {
        public string username_mediador { get; set; }
        public string username_cliente { get; set; }
        public int id_imovel { get; set; }
        public string dia { get; set; }
        public string hora_inicio { get; set; }
        public string hora_fim { get; set; }
    }
}
