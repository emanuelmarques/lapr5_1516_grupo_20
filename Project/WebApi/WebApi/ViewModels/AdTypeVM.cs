﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.ViewModels
{
    public class AdTypeVM
    {
        public int AdTypeID { get; set; }
        public string Description { get; set; }
    }
}
