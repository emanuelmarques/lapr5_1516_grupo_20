﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.ViewModels
{
    public class NotificationVM
    {
        public int NotificationID { get; set; }

        public string menssage { get; set; }

        public string userName { get; set; }

        public Boolean notified { get; set; }


        public NotificationVM Translate(Notification notification)
        {
            

            NotificationID = notification.NotificationID;
            menssage = notification.menssage;
            userName = notification.userName;
           notified = notification.notified;

            return this;

        }

        public Notification TranslateToNotification(NotificationVM notificationVM)
        {
            var notification = new Notification();

            notification.NotificationID = notificationVM.NotificationID;
            notification.menssage = notificationVM.menssage;
            notification.userName = notificationVM.userName;
            notification.notified = notificationVM.notified;

            return notification;
        }

    }

}
