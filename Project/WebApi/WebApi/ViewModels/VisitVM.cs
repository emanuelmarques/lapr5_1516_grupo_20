﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApi.ViewModels
{
    public class VisitVM
    {
        public int ID { get; set; }

        public DateTime Day { get; set; }

        public DateTime StartHour { get; set; }

        public DateTime EndHour { get; set; }

        public string User { get; set; }
        
        public int realtyID { get; set; }
        public string realty { get; set; }

        public VisitVM() { }

        public VisitVM setDados(Visit v)
        {
            ID = v.VisitID;
            Day = v.Day;
            StartHour = v.StartHour;
            EndHour = v.EndHour;
            User = v.UserID;
            realtyID = v.RealtyID;
            realty = v.Realty.RealtyType.Description;
            return this;
        }

        public Visit TranslateToVisit(Visit v, VisitVM visitVM )
        {
            v.VisitID = visitVM.ID;
            v.Day = visitVM.Day;
            v.StartHour = visitVM.StartHour;
            v.EndHour = visitVM.EndHour;
            v.UserID = visitVM.User;
            
            return v;
        }

        public Visit TranslateToVisit(Visit v)
        {
            v.VisitID = this.ID;
            v.Day = this.Day;
            v.StartHour = this.StartHour;
            v.EndHour = this.EndHour;
            v.UserID = this.User;


            return v;
        }
    }
}
