﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RealEstateLibrary.Models;
using WebApi.ViewModels;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [Authorize]
    public class AdTypeController : ApiController
    {
        private AdTypeRepository AdType_repo = new AdTypeRepository();

        // GET: api/AdType
        public List<AdTypeVM> GetAdType()
        {
            var listAdTypes = AdType_repo.GetAllAdTypes();
            var listAdTypeVMs = new List<AdTypeVM>();

            for (var i = 0; i < listAdTypes.Count; i++)
            {
                var adTypeVM = AdTypeToAdTypeVM(listAdTypes[i]);
                listAdTypeVMs.Add(adTypeVM);
            }

            return listAdTypeVMs;
        }

        private AdTypeVM AdTypeToAdTypeVM(AdType at)
        {
            var atVM = new AdTypeVM();

            atVM.AdTypeID = at.AdTypeID;
            atVM.Description = at.Description;
       
            return atVM;
        }

        // GET: api/AdType/5
        [ResponseType(typeof(AdTypeVM))]
        public IHttpActionResult GetAdType(int id)
        {
            AdType adType = AdType_repo.GetAdTypeByID(id);
            if (adType == null)
            {
                return NotFound();
            }

            return Ok(adType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                AdType_repo._ctx.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AdTypeExists(int id)
        {
            return AdType_repo._ctx.AdType.Count(e => e.AdTypeID == id) > 0;
        }
    }
}