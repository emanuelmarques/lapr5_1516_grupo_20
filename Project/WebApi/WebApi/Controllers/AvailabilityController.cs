﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Repository;
using WebApi.ViewModels;

namespace WebApi.Controllers
{
    public class AvailabilityController : ApiController
    {
        private AvailabilityRepository availability_repo = new AvailabilityRepository();
       



        //GET: api/Visit
        public List<AvailabilityVM> GetAvailabilities()
        {
            return availability_repo.GetAllAvailabilitiesVM();
        }

        // GET: api/Availability/5
        [ResponseType(typeof(AvailabilityVM))]
        public IHttpActionResult GetAvailability(int id)
        {
            AvailabilityVM v = availability_repo.GetAvailabilityVMByID(id);
            if (v == null)
            {
                return NotFound();
            }

            return Ok(v);
        }

        // POST: api/Availability
        [ResponseType(typeof(Availability))]
        public IHttpActionResult PostAvailability(AvailabilityVM availabilityVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Availability v = new Availability();
           

            v = availabilityVM.TranslateToAvailability(v, availabilityVM);
            v.Realty = availability_repo._ctx.Realty.Where(im => im.RealtyID == v.RealtyID).ToList().ElementAt(0);

            availability_repo.InsertAvailability(v);
            availability_repo._ctx.SaveChanges();
           

            return CreatedAtRoute("DefaultApi", new { id = v.AvaibilityID }, v);
        }

        // PUT: api/Availability/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAvailability(int id, AvailabilityVM availabilityVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != availabilityVM.ID)
            {
                return BadRequest();
            }

            Delete(availabilityVM.ID);

            PostAvailability(availabilityVM);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Availability/5
        [ResponseType(typeof(Availability))]
        public IHttpActionResult Delete(int id)
        {
            Availability a = availability_repo.GetAvailabilityByID(id);
            if (a == null)
            {
                return NotFound();
            }

            availability_repo.Delete(a.AvaibilityID);
            availability_repo._ctx.SaveChanges();

            return Ok(a);


        }
        // GET: api/Availability?userID={userID}
        public List<AvailabilityVM> GetAvailabilitiesByUserID(string data, string userID)
        {
            return availability_repo.GetAvailabilityByUserID(data, userID);
        }

        // GET: api/Availability/GetClientes?IDMediador=IDMediador
        [Route("api/Availability/GetClientes/{data}/{IDMediador}")]
        public HttpResponseMessage GetClientes(string data, string IDMediador)
        {
            string clientes = availability_repo.getClientesQueremVisitarImoveis(data,IDMediador);
            return new HttpResponseMessage()
            {
                Content = new StringContent(
                    clientes,
                    Encoding.UTF8,
                    "text/html"
                )
            };
        }
        [Route("api/Availability/GetImoveis/{data}/{IDMediador}/{IDCliente}/")]
        public HttpResponseMessage GetImoveis(string data, string IDMediador, string IDCliente)
        {
            string imoveis = availability_repo.getImoveisQueClienteQuerVisitar(data,IDMediador,IDCliente);
            return new HttpResponseMessage()
            {
                Content = new StringContent(
                    imoveis,
                    Encoding.UTF8,
                    "text/html"
                )
            };
        }
        [Route("api/Availability/GetDisponibilidadesClienteImovel/{data}/{IDMediador}/{IDCliente}/{IDImovel}")]
        public List<AvailabilityVM> GetDisponibilidadesClienteImovel(string data,string IDMediador,string IDCliente,int IDImovel)
        {
            return availability_repo.getDisponibilidadesImoveisClienteQuerVisitar(data,IDMediador,IDCliente,IDImovel);



        }



    }
}
