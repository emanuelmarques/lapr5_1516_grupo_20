﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using WebApi.Repository;

namespace WebApi.Controllers
{
    public class UserController : ApiController
    {
        private UserRepository users_repo = new UserRepository();

        // GET: api/User
        public List<ApplicationUser> GetUsers()
        {

            var listUsers = users_repo.GetAllUsers();

            return listUsers;
        }
    }
}