﻿using Newtonsoft.Json;
using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using WebApi.Helpers;
using WebApi.Repository;
using WebApi.ViewModels;


namespace WebApi.Controllers
{
    public class AlgavController : ApiController
    {
        private VisitRepository visitRepo = new VisitRepository();
        private UserRepository userRepo = new UserRepository();
        private RealtyRepository realtyRepo = new RealtyRepository();
        private AvailabilityRepository avai_repo = new AvailabilityRepository();

        // GET: api/Algav
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Algav/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Algav
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Algav/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Algav/5
        public void Delete(int id)
        {
        }

        [System.Web.Http.Route("ServiceAlgav/Create/{data}/{user_mediador}")]
        [System.Web.Http.HttpGet]
        public async Task<HttpResponseMessage> Create(string data, string user_mediador)
        {

            var clientALGAV = WebApiALGAVHttpClient.GetClient();
            //VisitVM v = new VisitVM();
            //Availability a = new Availability();
            //a = avai_repo.GetAvailabilityByID(System.Web.HttpContext.Current.User.Identity.GetUserId());

            HttpResponseMessage response = await clientALGAV.GetAsync("apiAlgav/gerar_agenda?url=http://localhost:50371/api&idmediador=" + user_mediador + "&data=" + data);

            //HttpResponseMessage response = await clientALGAV.GetAsync("apiAlgav/gerar_agenda?url=http://localhost:50371/?idmediador=100&data=2016-01-01");
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                var algav_visit =
                JsonConvert.DeserializeObject<IEnumerable<AlgavVisitVM>>(content); // enumeravel de visitas(DTO) do mediador

                for (var i = 0; i < algav_visit.Count(); i++)
                {
                    if (algav_visit.ElementAt(i).id_imovel != null)
                    {
                        // guardar visita
                        Visit visita1 = new Visit();
                        //visita1.Realty = realtyRepo.GetRealtyByID(algav_visit.ElementAt(i).id_imovel);
                        visita1.RealtyID = algav_visit.ElementAt(i).id_imovel;
                       

                        string[] dia_string = algav_visit.ElementAt(i).dia.Split('T');
                        string dia2 = dia_string[0].Replace("'", ""); // dia em string

                        string[] horasi = algav_visit.ElementAt(i).hora_inicio.Split('T');
                       string[] hora_inicio_array = horasi[1].Split('.');
                        string hora_inicio = hora_inicio_array[0]; //hora inicio string

                        string[] horasf = algav_visit.ElementAt(i).hora_fim.Split('T');
                        string[] hora_fim_array = horasf[1].Split('.');
                        string hora_fim = hora_fim_array[0]; //hora fim string

                        DateTime dia = DateTime.ParseExact(dia2 + " 00:00:00", "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime start = DateTime.ParseExact(dia2 + " "+ hora_inicio, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime end = DateTime.ParseExact(dia2 + " " + hora_fim, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);


                        visita1.StartHour = start;
                        visita1.EndHour = end;
                        visita1.Day = dia;
                        visita1.UserID = algav_visit.ElementAt(i).username_cliente;


                        var visit = new Visit();

                        visit.RealtyID = algav_visit.ElementAt(i).id_imovel;
                        visit.StartHour = start;
                        visit.EndHour = end;
                        visit.Day = dia;


                        visit.UserID = algav_visit.ElementAt(i).username_mediador;

                        //this.visitRepo.InsertVisit(visit.ElementAt(i).TranslateToVisit(visita));
                        this.visitRepo.InsertVisit(visit);
                     
                        this.visitRepo.InsertVisit(visita1);
                        await this.visitRepo._ctx.SaveChangesAsync();
             

                        //TO DO: adicionar visita à agenda do mediador
                        //var user = this.userRepo.GetAllUsers().Where(u => u.UserName.Equals(user_mediador)).ToList().ElementAt(0);
                        //this.userRepo.addVisitToUserIDAgenda(visit, user);

                        //adicionar visita à agenda do imóvel
                        //this.realtyRepo.addVisitToRealtyAgenda(visit);
                        //this.realtyRepo._ctx.Visits.Add(visit);
                        //this.realtyRepo._ctx.Visits.Add(visita1);
                        //this.realtyRepo._ctx.SaveChanges();


                        //TO DO: adicionar visita à agenda do cliente
                        //this.userRepo.addVisitToUserAgenda(visita1);
                        //this.userRepo._ctx.SaveChanges();
                    }
                }

                //return System.Web.Mvc.ActionResult.
                //return View(visit);
                return new HttpResponseMessage(HttpStatusCode.OK);

            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.Conflict);
            }
        }
    }
}
