﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RealEstateLibrary.Models;
using WebApi.Repository;
using WebApi.ViewModels;
using Web_API.Repository;

namespace WebApi.Controllers
{
    [Authorize]
    public class RealtyTypeController : ApiController
    {
        private RealtyTypeRepository RT_repo = new RealtyTypeRepository();
        private AdRepository Ad_repo = new AdRepository();
        private AlertRepository Alert_repo = new AlertRepository();


        // GET: api/RealtyType
        public List<RealtyTypeVM> GetRealtyType()
        {
            var listRealtyTypes = RT_repo.GetAllRealtyTypes();
            var listRTVMs = new List<RealtyTypeVM>();

            for (var i = 0; i < listRealtyTypes.Count; i++)
            {
                var rtVM = RealtyTypeToRealtyTypeVM(listRealtyTypes[i]);
                listRTVMs.Add(rtVM);
            }

            return listRTVMs; 
        }

        private RealtyTypeVM RealtyTypeToRealtyTypeVM(RealtyType realtyType)
        {
            var rtVM = new RealtyTypeVM();

            rtVM.Description = realtyType.Description;           
            rtVM.RealtyTypeID = realtyType.RealtyTypeID;

            if (realtyType.MainTypeID != null)
            {
                rtVM.MainTypeDescription = realtyType.MainType.Description;
                rtVM.MainTypeID = realtyType.MainTypeID;
            }

            return rtVM;
        }

        // GET: api/RealtyType/5
        [ResponseType(typeof(RealtyTypeVM))]
        public IHttpActionResult GetRealtyType(int id)
        {
            RealtyType realtyType = RT_repo.GetRealtyTypeByID(id);
            if (realtyType == null)
            {
                return NotFound();
            }

            var rtVM = RealtyTypeToRealtyTypeVM(realtyType);

            return Ok(rtVM);
        }

        // PUT: api/RealtyType/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRealtyType(int id, RealtyTypeVM realtyTypeVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != realtyTypeVM.RealtyTypeID)
            {
                return BadRequest();
            }

            var realtyType = RealtyTypeVMToRealtyType(realtyTypeVM);

            RT_repo._ctx.Entry(realtyType).State = EntityState.Modified;

            try
            {
                RT_repo._ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RealtyTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private RealtyType RealtyTypeVMToRealtyType(RealtyTypeVM realtyTypeVM)
        {
            var rt = new RealtyType();

            rt.Description = realtyTypeVM.Description;
            if (realtyTypeVM.MainTypeID != null)
            {
                rt.MainType = RT_repo.GetRealtyTypeByID((int)realtyTypeVM.MainTypeID);
                rt.MainTypeID = realtyTypeVM.MainTypeID;
            }

            rt.RealtyTypeID = realtyTypeVM.RealtyTypeID;

            return rt;
        }

        // POST: api/RealtyType
        [ResponseType(typeof(RealtyType))]
        public IHttpActionResult PostRealtyType(RealtyTypeVM realtyTypeVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var realtyType = RealtyTypeVMToRealtyType(realtyTypeVM);

            RT_repo.InsertRealtyType(realtyType);
            RT_repo._ctx.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = realtyType.RealtyTypeID }, realtyType);
        }

        // DELETE: api/RealtyType/5
        [ResponseType(typeof(RealtyType))]
        public IHttpActionResult DeleteRealtyType(int id)
        {
            RealtyType realtyType = RT_repo.GetRealtyTypeByID(id);
            if (realtyType == null)
            {
                return NotFound();
            }

            List<RealtyType> list_realty_types = RT_repo.GetAllRealtyTypes();
            List<RealtyType> listApagar = new List<RealtyType>();

            for (int i = 0; i < list_realty_types.Count; i++)
            {
                if (list_realty_types[i].MainTypeID == id)
                {
                    listApagar.Add(list_realty_types[i]);
                }
            }

            Boolean flag = true;
            List<Ad> ad1 = Ad_repo.GetAllAds();

            for (int i = 0; i < listApagar.Count; i++)
            {
                int id2 = listApagar[i].RealtyTypeID;
                List<Ad> ad2 = Ad_repo.GetAllAds().Where(a => a.Realty.RealtyTypeID.Equals(id2)).ToList();
                if (ad2.Count > 0)
                {
                    flag = false;
                    i = listApagar.Count;
                }
            }

            if (ad1.Count > 0 || !flag)
            {
                return NotFound();
            }
            else
            {

                List<Alert> list_alerts = Alert_repo.GetAllAlerts();

                listApagar.Add(realtyType);

                for (int i = 0; i < list_alerts.Count; i++)
                {
                    for (int j = 0; j < listApagar.Count; j++)
                    {

                        if (list_alerts[i].RealtyTypeID == listApagar[j].RealtyTypeID)
                        {
                            Alert_repo.DeleteAlert(list_alerts[i].AlertID);
                        }
                    }
                }

                for (int j = 0; j < listApagar.Count; j++)
                {
                    RT_repo.DeleteRealtyType(listApagar[j].RealtyTypeID);
                }

            }

            Ad_repo._ctx.SaveChanges();
            Alert_repo._ctx.SaveChanges();
            RT_repo._ctx.SaveChanges();

            return Ok(realtyType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                RT_repo._ctx.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RealtyTypeExists(int id)
        {
            return RT_repo._ctx.RealtyType.Count(e => e.RealtyTypeID == id) > 0;
        }
    }
}