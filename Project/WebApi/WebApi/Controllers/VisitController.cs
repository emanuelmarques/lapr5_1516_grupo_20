﻿using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Repository;
using WebApi.ViewModels;

namespace WebApi.Controllers
{
   // [Authorize]
    public class VisitController : ApiController
    {
        private VisitRepository visit_repo = new VisitRepository();
        private RealtyRepository realty_repo = new RealtyRepository();
        


        //GET: api/Visit
        public List<VisitVM> GetVisits()
        {
            return visit_repo.GetAllVisitsVM();
        }

        // GET: api/Visit/5
        [ResponseType(typeof(VisitVM))]
        public IHttpActionResult GetVisit(int id)
        {
            VisitVM v = visit_repo.GetVisitVMByID(id);
            if (v == null)
            {
                return NotFound();
            }

            return Ok(v);
        }

        // POST: api/Visit
        [ResponseType(typeof(Visit))]
        public IHttpActionResult PostVisit(VisitVM visitVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Visit v = new Visit();
           
            v = visitVM.TranslateToVisit(v,visitVM);

            v.Realty = realty_repo.GetRealtyByID(visitVM.realtyID);
            v.UserID = User.Identity.Name;

            visit_repo.InsertVisit(v);
            visit_repo._ctx.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = v.VisitID }, v);
        }

        // PUT: api/Visit/5
        public void PutVisit(int id, VisitVM visitVM)
        {
            
        }

        // DELETE: api/Visit/5
        public void Delete(int id)
        {
        }
    }
}
