﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RealEstateLibrary.Models;
using WebApi.Repository;
using WebApi.ViewModels;
using Web_API.Repository;
using WebApi.Service;
using System.Web.Http.Description;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace WebApi.Controllers
{
    public class NotificationController : ApiController
    {
        private NotificationRepository notify_repo = new NotificationRepository();
        // GET: api/Notification
        public List<NotificationVM> Get()
        {

            return notify_repo.GetAllNotificationsVM();
        }

        // GET: api/Notification/5
        public NotificationVM Get(int id)
        {
            return notify_repo.GetNotificationVMByID(id);
        }

        // POST: api/Notification
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Notification/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Put(int id, NotificationVM NotificationVM)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != NotificationVM.NotificationID)
            {
                return BadRequest();
            }

            var notitifyVM = NotificationVM;
            var notification = NotificationVM.TranslateToNotification(notitifyVM);

           // alert.UserName = User.Identity.Name;

            notify_repo._ctx.Entry(notification).State = EntityState.Modified;

            try
            {
                notify_repo._ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!notify_repo.Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Notification/5
        public void Delete(int id)
        {
        }
    }
}
