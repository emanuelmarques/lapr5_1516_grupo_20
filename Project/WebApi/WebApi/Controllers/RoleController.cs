﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using RealEstateLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using WebApi.ViewModels;
using WebApi.Repository;

namespace WebApi.Controllers
{
    
    public class RoleController : ApiController
    {


        private RoleRepository roles_repo = new RoleRepository();

        // GET: api/Roles
        public List<RoleVM> GetRoles()
        {

            var listRoles = roles_repo.GetAllRoles();

            var listRolesVM = new List<RoleVM>();

            for (var i=0; i < listRoles.Count; i++)
            {
                listRolesVM.Add(RoleToRoleVM(listRoles[i]));
            }

            return listRolesVM;
        }

        private RoleVM RoleToRoleVM(IdentityRole identityRole)
        {
            RoleVM rvm = new RoleVM();

            rvm.name = identityRole.Name;

            return rvm;
        }


        // POST: api/Roles
        [ResponseType(typeof(IdentityRole))]
        public IHttpActionResult PostRole(RoleVM rvm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityRole role = RoleVMToRole(rvm);
            roles_repo.InsertRole(role);
            roles_repo._ctx.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = role.Id }, role);

        }

        private IdentityRole RoleVMToRole(RoleVM rvm)
        {
            IdentityRole role = new IdentityRole();
            role.Name = rvm.name;

            return role;
        }

        // DELETE: api/Roles/5
        [ResponseType(typeof(IdentityRole))]
        public IHttpActionResult DeleteRole(String name)
        {
            var roles = roles_repo.GetAllRoles();

            IdentityRole role = null;

            for (var i=0; i < roles.Count; i++)
            {
                if (roles[i].Name.Equals(name))
                {
                    role = roles[i];
                }
            }

            if (role == null)
            {
                return NotFound();
            }

            roles_repo.DeleteRole(role);

            roles_repo._ctx.SaveChanges();

            return Ok();

        }

        [ResponseType(typeof(IdentityRole))]
        [System.Web.Http.Route("api/addRoleToUser")]
        public IHttpActionResult PutAddRoleToUser(RoleVM rvm)
        {
            var roleStore = new RoleStore<IdentityRole>(roles_repo._ctx);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            var userStore = new UserStore<ApplicationUser>(roles_repo._ctx);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var user = userManager.FindByName(rvm.user);
            if (user == null)
                throw new Exception("User not found!");

            var role = roleManager.FindByName(rvm.name);
            if (role == null)
                throw new Exception("Role not found!");

            userManager.AddToRole(user.Id, role.Name);
            roles_repo._ctx.SaveChanges();

            return Ok();

        }

        public List<RoleVM> GetUserRoles(string userName)
        {

            var listRoles = roles_repo.GetAllRoles();

            var listRolesVM = new List<RoleVM>();

            List<IdentityRole> UserRoles = new List<IdentityRole>();

            using (var context = roles_repo._ctx)
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var user = userManager.FindByName(userName);
                if (user == null)
                    throw new Exception("User not found!");

                for (var i = 0; i < listRoles.Count; i++)
                {
                    if (userManager.IsInRole(user.Id, listRoles[i].Name))
                    {
                        UserRoles.Add(listRoles[i]);
                    }
                }

            }

            for (var i = 0; i < UserRoles.Count; i++)
            {
                listRolesVM.Add(RoleToRoleVM(UserRoles[i]));
            }

            return listRolesVM;
        }

        [ResponseType(typeof(IdentityRole))]
        public IHttpActionResult DeleteRoleForUser(string userName, string roleName)
        {
            List<string> roles;
            List<string> users;
            using (var context = roles_repo._ctx)
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);

                roles = (from r in roleManager.Roles select r.Name).ToList();

                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                users = (from u in userManager.Users select u.UserName).ToList();

                var user = userManager.FindByName(userName);
                if (user == null)
                    throw new Exception("User not found!");

                if (userManager.IsInRole(user.Id, roleName))
                {
                    userManager.RemoveFromRole(user.Id, roleName);
                    context.SaveChanges();

                }
            }

            return Ok();
        }

    }
}