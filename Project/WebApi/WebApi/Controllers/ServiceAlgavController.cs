﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebApi.Helpers;
using WebApi.ViewModels;
using Microsoft.AspNet.Identity;
using RealEstateLibrary.Models;
using WebApi.Repository;

namespace WebApi.Service
{
    public class ServiceAlgavController : Controller
    {

        private VisitRepository visitRepo = new VisitRepository();
        private UserRepository userRepo = new UserRepository();
        private RealtyRepository realtyRepo = new RealtyRepository();
        private AvailabilityRepository avai_repo = new AvailabilityRepository();

        // GET: ServiceAlgav
        public ActionResult Index()
        {
            return View();
        }

        // GET: ServiceAlgav/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

    
       

        // POST: ServiceAlgav/Create
        [HttpPost]
        public async Task<ActionResult> Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ServiceAlgav/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ServiceAlgav/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: ServiceAlgav/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ServiceAlgav/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
