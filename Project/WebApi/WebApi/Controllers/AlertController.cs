﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using RealEstateLibrary.Models;
using WebApi.Repository;
using WebApi.ViewModels;
using Web_API.Repository;
using WebApi.Service;

namespace WebApi.Controllers
{
    [Authorize]
    public class AlertController : ApiController
    {
        private AlertRepository alert_repo = new AlertRepository();
        private AdTypeRepository adType_repo = new AdTypeRepository();
        private RealtyTypeRepository realtyType_repo = new RealtyTypeRepository();
        private AdRepository ad_repo = new AdRepository();
        private VerifyMatchNotification match = new VerifyMatchNotification();


        // GET: api/Alert
        public List<AlertVM> GetAlert()
        {
            var listAlerts = alert_repo.GetAllAlerts();
            var listAlertVMs = new List<AlertVM>();

            for (var i = 0; i < listAlerts.Count; i++)
            {
                var alertVM = AlertToAlertVM(listAlerts[i]);
                listAlertVMs.Add(alertVM);
            }

            return listAlertVMs;
        }

        private AlertVM AlertToAlertVM(Alert alert)
        {
            var alertVM = new AlertVM();

            if (alert.AdTypeID != null)
            {
                alertVM.AdTypeID = alert.AdTypeID;
            }

            if (alert.RealtyTypeID != null)
            {
                alertVM.RealtyTypeID = alert.RealtyTypeID;
            }

            alertVM.Address = alert.Address;
            alertVM.AlertID = alert.AlertID;
            alertVM.MaxArea = alert.MaxArea;
            alertVM.MaxCost = alert.MaxCost;
            alertVM.UserName = alert.UserName;

            return alertVM;
        }

        // GET: api/Alert/5
        [ResponseType(typeof(AlertVM))]
        public IHttpActionResult GetAlert(int id)
        {
            Alert alert = alert_repo.GetAlertByID(id);
            if (alert == null)
            {
                return NotFound();
            }

            var alertVM = AlertToAlertVM(alert);

            return Ok(alertVM);
        }

        // PUT: api/Alert/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAlert(int id, AlertVM alertVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != alertVM.AlertID)
            {
                return BadRequest();
            }

            var alert = AlertVMToAlert(alertVM);

            alert.UserName = User.Identity.Name;

            alert_repo._ctx.Entry(alert).State = EntityState.Modified;

            try
            {
                alert_repo._ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlertExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        private Alert AlertVMToAlert(AlertVM alertVM)
        {
            var alert = new Alert();

            if (alertVM.AdTypeID != null)
            {
                alert.AdTypeID = alertVM.AdTypeID;
            }

            if (alertVM.RealtyTypeID != null)
            {
                alert.RealtyTypeID = alertVM.RealtyTypeID;
            }

            alert.Address = alertVM.Address;
            alert.AlertID = alertVM.AlertID;
            alert.MaxArea = alertVM.MaxArea;
            alert.MaxCost = alertVM.MaxCost;
            alert.UserName = alertVM.UserName;

            return alert;
        }

        // POST: api/Alert
        [ResponseType(typeof(Alert))]
        public IHttpActionResult PostAlert(AlertVM alertVM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var alert = AlertVMToAlert(alertVM);
            alert.UserName = User.Identity.Name;

            alert_repo.InsertAlert(alert);
            alert_repo._ctx.SaveChanges();

            
            alert_repo.Subscribe(match);

            return CreatedAtRoute("DefaultApi", new { id = alert.AlertID }, alert);
        }

        // DELETE: api/Alert/5
        [ResponseType(typeof(Alert))]
        public IHttpActionResult DeleteAlert(int id)
        {
            Alert alert = alert_repo.GetAlertByID(id);
            if (alert == null)
            {
                return NotFound();
            }

            alert_repo.DeleteAlert(alert.AlertID);
            alert_repo._ctx.SaveChanges();

            return Ok(alert);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                alert_repo._ctx.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AlertExists(int id)
        {
            return alert_repo._ctx.Alert.Count(e => e.AlertID == id) > 0;
        }
    }
}