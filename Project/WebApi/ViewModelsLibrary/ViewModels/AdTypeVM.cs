﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelsLibrary.ViewModels
{
    public class AdTypeVM
    {
        public int AdTypeID { get; set; }
        public string Description { get; set; }
    }
}
