﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModelsLibrary.ViewModels
{
    public class RealtyTypeVM
    {
        public int RealtyTypeID { get; set; }
        public string Description { get; set; }

        public int? MainTypeID { get; set; }
        public virtual string MainTypeDescription { get; set; }
    }
}
