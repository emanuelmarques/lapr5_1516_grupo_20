using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace ViewModelsLibrary.ViewModels
{
    public class AlertVM
    {
        public int AlertID { get; set; }

        public int? AdTypeID { get; set; }
        public string AdTypeDescription { get; set; }

        public int? RealtyTypeID { get; set; }
        public string RealtyTypeDescription { get; set; }

        public string Address { get; set; }
        public decimal? MaxCost { get; set; }
        public decimal? MaxArea { get; set; }

        public string UserName { get; set; }

        public Boolean isProprietary(string u)
        {
            if (u.Equals(this.UserName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}