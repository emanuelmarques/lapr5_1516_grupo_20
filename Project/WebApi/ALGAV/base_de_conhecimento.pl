zona(a,hospital_Sao_Joao).
zona(b,areosa).
zona(c ,prelada).
zona(d, paranhos).
zona(e, contumil).
zona(f ,boavista).
zona(g ,antas).
zona(h ,fonte_da_Moura).
zona(i ,foz_do_Douro).
zona(j, campanha).
zona(l, massarelos).
zona(m, lordelo_do_Ouro).
zona(n, campo_24_Agosto).
zona(o, azevedo).
zona(p, vitoria).
zona(q, freixo).
zona(r, fontainhas).

%Dist�ncias
estrada(a,b,7).
estrada(a,c,8).
estrada(a,d,5).
estrada(a,e,12).
estrada(b,e,6).
estrada(d,e,7).
estrada(c,d,8).
estrada(c,f,13).
estrada(c,i,18).
estrada(d,f,15).
estrada(f,h,3).
estrada(f,m,4).
estrada(f,i,6).
estrada(i,m,5).
estrada(e,f,12).
estrada(e,g,2).
estrada(e,j,5).
estrada(g,h,16).
estrada(g,l,18).
estrada(g,j,6).
estrada(h,m,4).
estrada(h,n,15).
estrada(h,l,3).
estrada(j,l,16).
estrada(j,o,3).
estrada(l,n,18).
estrada(l,o,20).
estrada(m,n,15).
estrada(m,p,10).
estrada(n,p,11).
estrada(n,r,12).
estrada(o,n,3).
estrada(o,q,5).
estrada(p,r,4).

%Coordenadas

localizacao(a,45,95).
localizacao(b,90,95).
localizacao(c,15,85).
localizacao(d,40,80).
localizacao(e,70,80).
localizacao(f,25,65).
localizacao(g,65,65).
localizacao(h,45,55).
localizacao(i,5,50).
localizacao(j,80,50).
localizacao(l,65,45).
localizacao(m,25,40).
localizacao(n,55,30).
localizacao(o,80,30).
localizacao(p,25,15).
localizacao(q,80,15).
localizacao(r,55,10).

% imovel(id, zona, tipologia, valor, temChave, listaDisponibilidades) em
% que listaDisponibilidades: (diaSemana, tempoInicio, TempoFim)
imovel(1,prelada,t2,s,[]).
imovel(2,paranhos,t3,s,[]).
imovel(3,contumil,t4,s,[]).

cliente(100,"Tomas Torres",[(2, 17:00, 20:30), (5, 14:00, 18:30),(7, 10:00,14:00), (1, 9:00, 12:00)]).
cliente(200,"Pedro",[(5, 17:00, 20:30), (8, 14:00, 18:30)]).
cliente(300,"Jeremias",[(3, 17:00, 20:30), (4, 14:00, 18:30)]).

%rela��o cliente imovel.
%imoveis_a_visitar(idCLiente, listaImoveis).
imoveis_a_visitar(c1001, [i1001,i1005]).

%mediador(id, nome, listaDisponibilidades: (dia, tempoInicio, TempoFim), etc.).
mediador(m1001, "Zacarias Lopes", [(2, 9:00, 17:00), (3, 9:00, 17:00)]).