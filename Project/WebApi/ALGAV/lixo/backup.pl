% M�TODO POST enviando um ficheiro de texto
% http_client:http_post('http://localhost:5000/send_file_post',
% form_data([file=file('./teste.txt')]), Reply, []).

send_file_post(Request) :-
	http_parameters(Request,[ file(X,[])]),
	format('Content-type: text/plain~n~n'),
	format('Received: ~w~n',[X]),
	open(X, read, Str),
	read_file(Str,'').