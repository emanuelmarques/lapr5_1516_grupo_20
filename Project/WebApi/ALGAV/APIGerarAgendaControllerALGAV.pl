% Bibliotecas
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_client)).
:- use_module(library(http/json)).
:- use_module(library(http/http_open)).
:- use_module(library(lists)).

% Rela��o entre pedidos HTTP e predicados que os processam
:- http_handler('/apiAlgav', responde_ola, []).
:- http_handler('/register_user', register_user, []).
%:- http_handler('/send_file_post', send_file_post, []).
:- http_handler('/apiAlgav/gerar_agenda',gerar_agenda, []).

% Importa��o do ficheiro para leitura dos dados
%:-consult(readLineByLineFromFile).
% Cria��o de servidor HTTP no porto 'Port'
server(Port) :-
        http_server(http_dispatch, [port(Port)]).

% Tratamento de 'http://localhost:5000/apiAlgav'
responde_ola(_Request) :-
        format('Content-type: text/plain~n~n'),
        format('Ol� LAPR5!~n').

% M�TODO GET: Tratamento de 'http://localhost:5000/RegisterUser?name='Jos�'&sex=male&birth_year=1975'
% ou http_client:http_get('http://localhost:5000/register_user?name=\'Jos�\'&sex=male&birth_year=1975',X,[]).

% M�TODO POST
% http_client:http_post('http://localhost:5000/register_user', form_data([name='Jos�', sex=male, birth_year=1975]), Reply, []).
:-dynamic utilizador/3.

register_user(Request) :-
    http_parameters(Request,
                    [ id(Id, []),
                      email(Email, []),
                      password(Password,[])
                    ]),
    %assertz(utilizador(Id,Email,Password)),
    format('Content-type: text/plain~n~n'),
    format('User registered!~n'),
    format('Id: ~w~nEmail: ~w~n',[Id,Email]).

% M�TODO POST
% http_client:http_post('http://localhost:5000/register_user', form_data([name='Jos�', sex=male, birth_year=1975]), Reply, []).
%
%gerar_agenda(Request):-
%	http_parameters(Request,[id(Id,[]),email(Email,[])

%http://stackoverflow.com/questions/29167342/prolog-http-get-request-with-request-headers

%Dict is a  SWI7 dictionary
%http://www.swi-prolog.org/pldoc/man?section=dicts

httpget_DisponibilidadesClienteImovel(Dict,URL):-
    setup_call_cleanup(
    http_open(URL, In, [request_header('Accept'='application/json')]),
    json_read_dict(In, Dict),
    close(In)).

httpget_DisponibilidadesMediador(URL_BASE,IDMEDIADOR,Data,Dict):-
	atomics_to_string([URL_BASE,'?data=',Data,'?userID=',IDMEDIADOR],'',URL),
	setup_call_cleanup(
	http_open(URL, In, [request_header('Accept'='application/json')]),
        json_read_dict(In, Dict),
        close(In)).


%    Disponibilidade Recebida
%    "ID": 1,
%    "Day": "2016-01-04T23:01:29.0857541+00:00",
%    "StartHour": "2016-01-04T23:01:29.0857541+00:00",
%    "EndHour": "2016-01-04T23:01:29.0857541+00:00",
%    "User": "sample string 5"

%atomics_to_string(+List, +Separator, -String)
%nth0(?Index, ?List, ?Elem)
split(String,Separator,Index,Elem):-atomics_to_string(List,Separator,String),nth0(Index,List,Elem).

%para cada cliente procura as suas disponibilidades
getDisponibilidades([],[]):-!.
getDisponibilidades([Dict|L],[A|Lista]):-
					      split(Dict.'Day',"T",0,Data),
					      split(Dict.'StartHour',"T",1,SH),
					      split(SH,":",0,HI),
					      split(SH,":",1,MI),
					      split(Dict.'EndHour',"T",1,EH),
					      split(EH,":",0,HF),
					      split(EH,":",1,MF),
					      StartHour=HI:MI,
					      EndHour=HF:MF,
					      A=..[disponibilididade,date(Data),time(StartHour),time(EndHour)],
					      getDisponibilidades(L,Lista).

%---

%getClientes(URL_BASE)




%---
%api/Availability/GetDisponibilidadesClienteImovel/{data}/{IDMediador}/{IDCliente}/{IDImovel}"
para_cada_imovel([],_,_,_,_).
para_cada_imovel([Imovel|L],URL_BASE,Data,IDMediador,IDCliente):-writeln(URL_BASE),
	                                             atomics_to_string([URL_BASE,'/GetDisponibilidadesClienteImovel/',Data,'/',IDMediador,'/',IDCliente,'/',Imovel,'/'],"",URL),writeln(URL),
						     httpget_DisponibilidadesClienteImovel(Dict,URL),
						     writeln(Dict),
						     getDisponibilidades(Dict,Lista),
						     R=..[cliente_imovel,IDCliente,Imovel,Lista],
						     assertz(R),
						     para_cada_imovel(L,URL_BASE,Data,IDMediador,IDCliente).


carregar_imoveis(URL_BASE,Data,IDMediador,IDCliente):-atomics_to_string([URL_BASE,'/GetImoveis/',Data,'/',IDMediador,'/',IDCliente,'/'],"",URL),

						      http_get(URL,In,[]),

						      atomics_to_string(Lista,";",In),
						      para_cada_imovel(Lista,URL_BASE,Data,IDMediador,IDCliente).


%---
para_cada_cliente([],_,_,_):-!.
para_cada_cliente([C|L],URL_BASE,Data,IDMediador):-carregar_imoveis(URL_BASE,Data,IDMediador,C),
						   !,
	                                           para_cada_cliente(L,URL_BASE,Data,IDMediador).

% para_cada_cliente([_|L],URL_BASE,Data,IDMediador):-para_cada_cliente(L,URL_BASE,Data,IDMediador).

criar_clientes([]):-!.
criar_clientes([C|L]):-Cliente =..[cliente,C],assertz(Cliente),criar_clientes(L).

%---

criar_mediador(IDMediador,Dict):-
			      getDisponibilidades(Dict,Lista)
			      ,A=..[mediador,IDMediador,Lista]
			      ,assertz(A).
%---
%pedido http /GetClientes/{data}/{IDMediador}
carregar_clientes(URL_BASE,Data,IDMediador):-
				             atomics_to_string([URL_BASE,'/GetClientes/',Data,'/',IDMediador,'/'],"",URL),
				             writeln(URL),
					     http_get(URL,In,[]),
					     writeln(In),
					     atomics_to_string(Lista,";",In),
	                                     criar_clientes(Lista),
					     writeln(Lista),
					     para_cada_cliente(Lista,URL_BASE,Data,IDMediador).

carregar_mediador(URL,Data,IDMediador):-httpget_DisponibilidadesMediador(URL,IDMediador,Data,Dict),criar_mediador(IDMediador,Dict).

%metodo que gera a agenda do mediador
gerar_agenda(Request):-
	               http_parameters(Request,[url(URL_BASE,[]),idmediador(Id,[]),data(Data,[])]) %vai receber tambem a data
		       ,carregar_mediador(URL_BASE,Data,Id)
		       ,carregar_clientes(URL_BASE,Data,Id)
		       %,gerar_zonas()
		       %,gerar_estradas()
		       ,criarVisitas(Visitas)
		       ,format('Status:~w~n',200)
		       ,format('Content-type: text/plain~n~n')
		       ,format('Agenda gerada com sucesso!').




			   %cliente(ID,ListaDisponibilidades).
			   %mediador(ID,ListaDisponibilidades).
			   %relacao_cliente_imoveis(ID,ListaImoveis).
			   %imovel(ID,TemChave,ListaDisponibilidades).

			   %relacao_cliente_imoveis
			   %IdCliente, imovel.

			  % disponibilidade_imovel

			   %17-05-2015, 17:00,18:00.
			   %18-05-2015, 17:00,18:00.
			   %id100,id200

			   %idUser

