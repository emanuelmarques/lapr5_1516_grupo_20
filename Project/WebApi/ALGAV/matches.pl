% Compara�ao de dois tempos, se as horas forem iguais compara os
% minutos, se nao forem, compara as horas.
earlier(H:M1, H:M2) :- !, M1 < M2.
earlier(H1:_, H2:_) :- H1 < H2, write("passou "),write(H1),writeln(H2).

%HIM-hora de inicio do mediador
%HFM-hora de fim do mediador
%HIC-hora de inicio do cliente
%HFC-hora de fim do cliente
% 4 possibilidades de interseca�ao entre os intervalos de tempo da
% disponibilidade
compatibilidade_horas(HIM,HFM,HIC,HFC,HIM,HFC):-earlier(HIC,HIM),earlier(HFC,HFM),earlier(HIM,HFC).
compatibilidade_horas(HIM,HFM,HIC,HFC,HIC,HFC):-earlier(HIM,HIC),earlier(HFC,HFM).
compatibilidade_horas(HIM,HFM,HIC,HFC,HIC,HFM):-earlier(HIM,HIC),earlier(HIC,HFM),earlier(HFM,HFC).
compatibilidade_horas(HIM,HFM,HIC,HFC,HIM,HFM):-earlier(HIC,HIM),earlier(HFM,HFC).

%Quando as horas de inicio sao iguais.
compatibilidade_horas(HIM,HFM,HIM,HFC,HIM,HFC):-earlier(HFC,HFM).
compatibilidade_horas(HIM,HFM,HIM,HFC,HIM,HFM):-earlier(HFM,HFC).

%Quando as horas de fim sao iguais.
compatibilidade_horas(HIM,HFM,HIC,HFM,HIC,HFM):-earlier(HIM,HIC).
compatibilidade_horas(HIM,HFM,HIC,HFM,HIM,HFM):-earlier(HIC,HIM).

%Quando as horas de fim e in�cio sao iguais.
compatibilidade_horas(HIM,HFM,HIM,HFM,HIM,HFM).

% match, correspond�ncia entre a disponibilidade do mediador e a
% disponibilidade do cliente.
% match(IDMEDIADOR,IDCLIENTE,HORAINICIO,HORAFIM)
:-dynamic match/4.

%IDM -ID do Mediador
%IDC -ID do Cliente
%HIM -Hora de In�cio do Mediador
%HFM -Hora de Fim do Mediador
%HIC -Hora de In�cio do Cliente
%HFC -Hora de Fim do Cliente
criar_match(IDM,IDC,(DIA,HIM,HFM),(D,HIC,HFC)):-
	DIA==D
	,compatibilidade_horas(HIM,HFM,HIC,HFC,HIF,HFF)
	,assertz(match(IDM,IDC,HIF,HFF)).

% iterar todas disponibilidades e encontrar o seu match com a
% disponibilidade do cliente

para_cada_disp_cliente(_,_,_,[]):-!.
para_cada_disp_cliente(IDM,DM,IDC,[DC|LDC]):-
					   criar_match(IDM,IDC,DM,DC),!
					   ,para_cada_disp_cliente(IDM,DM,IDC,LDC).

para_cada_disp_cliente(IDM,DM,IDC,[_|LDC]):-
	                                   para_cada_disp_cliente(IDM,DM,IDC,LDC).

%percorrer todos os clientes para encontrar as suas disponibilidades
para_cada_cliente(_,_,[]).
para_cada_cliente(IDM,DM,[(IDC,LDC)|LC]):-writeln(LDC),writeln(DM),para_cada_disp_cliente(IDM,DM,IDC,LDC),para_cada_cliente(IDM,DM,LC).

para_cada_disp_mediador(_,[],_).
para_cada_disp_mediador(IDM,[DM|L],LDC):-para_cada_cliente(IDM,DM,LDC),para_cada_disp_mediador(IDM,L,LDC).

%recebe o id do mediador.
%LDC - Lista de disponibilidades do cliente
%LDCs
criar_matches(IDMEDIADOR):-findall((IDCLIENTE,LDC),cliente(IDCLIENTE,_,LDC),LISTA),
		 mediador(IDMEDIADOR,_,LDM),
		 retractall(match(_,_,_,_)),
		 para_cada_disp_mediador(IDMEDIADOR,LDM,LISTA),fail;true.
