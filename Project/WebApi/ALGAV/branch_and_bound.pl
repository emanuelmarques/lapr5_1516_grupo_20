:-consult(base_de_conhecimento).

elimina1(_,[],[]).
elimina1(X,[X|L],L):-!.
elimina1(X,[Y|L],[Y|L1]):-elimina1(X,L,L1).

% verifica se a segunda lista contem a primeira, recebidas por
% parametros.
esta_contida([ ],[ ]).
esta_contida(L,[X|L1]):-elimina1(X,L,Li),esta_contida(Li,L1).

menor_percursoh([X],X):-!.
menor_percursoh([c(F/_,_)|L],c(FM/GM,P)):-Menor=c(FM/GM,P),menor_percursoh(L,Menor),FM<F,!.
menor_percursoh([X|_],X).

bb(Orig,VerticesVisitar,Perc,Total):-
			%estimativa(Orig,Dest,H), F is H + 0, % G = 0.
			%hbf1([c(F/0,[Orig])],Dest,P,Total),
			bb1([c(0/0,[Orig])],VerticesVisitar,P,Total),
			reverse(P,Perc).
%select(?Elem, ?List1, ?List2).
bb1(Percursos,ZonasVisitar,Percurso,Total):-
	                %menor_percursoh(Percursos,Menor),Restantes),
                        menor_percursoh(Percursos,Menor),
			select(Menor,Percursos,Restantes),
			percursos_seguintesh(Menor,ZonasVisitar,Restantes,Percurso,Total).

percursos_seguintesh(c(_/Dist,Percurso),ZonasVisitar,_,Percurso,Dist):-esta_contida(ZonasVisitar,Percurso).%,!.%Percurso=[Dest|_],!.
%verifica se o primeiro elemento do percurso � o destino.
percursos_seguintesh(c(_,P2),ZonasVisitar,Restantes,Percurso,Total):-esta_contida(ZonasVisitar,P2),!,bb1(Restantes,ZonasVisitar,Percurso,Total).
% procura outras solu��es se encontrou o caminho.
percursos_seguintesh(c(_/Dist,[Ult|T]),ZonasVisitar,Percursos,Percurso,Total):-

	findall(c(F1/D1,[Z,Ult|T]),proximo_noh(Ult,T,Z,Dist,ZonasVisitar,F1/D1),Lista),
	%junta z ao ult que foi visitado, passa a proximo n� F1/D1, Dist �
	append(Lista,Percursos,NovosPercursos),
	bb1(NovosPercursos,ZonasVisitar,Percurso,Total).



proximo_noh(X,T,Y,Dist,ZonasVisitar,F/Dist1):-
					(estrada(X,Y,Z);estrada(Y,X,Z)),
					\+ member(Y,T),
					Dist1 is Dist + Z,
					estimativa(Y,ZonasVisitar,H), F is H + Dist1.

%estimativa(C1,C2,Est):-
%		localizacao(C1,X1,Y1),
%		localizacao(C2,X2,Y2),
%		DX is X1-X2,
%		DY is Y1-Y2,
%		Est is sqrt(DX*DX+DY*DY).

estimativa(_,_,0). % para desprezar a heur�stica.
