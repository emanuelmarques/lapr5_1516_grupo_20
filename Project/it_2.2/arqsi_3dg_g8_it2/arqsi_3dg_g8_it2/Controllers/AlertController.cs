﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RealEstateLibrary.Models;
using Microsoft.AspNet.Identity;

namespace arqsi_3dg_g8_it2.Controllers
{
    public class AlertController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Alert
        public ActionResult Index()
        {
            var alert = db.Alert.Include(a => a.AdType).Include(a => a.RealtyType);
            return View(alert.ToList());
        }

        // GET: Alert/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alert.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        // GET: Alert/Create
        public ActionResult Create()
        {
            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description");
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description");
            return View();
        }

        // POST: Alert/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AlertID,AdTypeID,RealtyTypeID,Address,MaxCost,MaxArea,UserName")] Alert alert)
        {
            if (ModelState.IsValid)
            {
                alert.UserName = User.Identity.Name;

                db.Alert.Add(alert);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description", alert.AdType);
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", alert.RealtyType);
            return View(alert);
        }

        // GET: Alert/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alert.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description", alert.AdType);
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", alert.RealtyType);
            return View(alert);
        }

        // POST: Alert/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AlertID,AdTypeID,RealtyTypeID,Address,MaxCost,MaxArea,UserName")] Alert alert)
        {
            if (ModelState.IsValid)
            {
                alert.UserName = User.Identity.GetUserId();

                db.Entry(alert).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description", alert.AdType);
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", alert.RealtyType);

            return View(alert);
        }

        // GET: Alert/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alert alert = db.Alert.Find(id);
            if (alert == null)
            {
                return HttpNotFound();
            }
            return View(alert);
        }

        // POST: Alert/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alert alert = db.Alert.Find(id);
            db.Alert.Remove(alert);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
