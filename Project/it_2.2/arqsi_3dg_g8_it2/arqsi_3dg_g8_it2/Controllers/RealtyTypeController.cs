﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RealEstateLibrary.Models;

namespace arqsi_3dg_g8_it2.Controllers
{
    public class RealtyTypeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RealtyType
        public ActionResult Index()
        {
            var realtyType = db.RealtyType.Include(r => r.MainType);
            return View(realtyType.ToList());
        }

        // GET: RealtyType/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealtyType realtyType = db.RealtyType.Find(id);
            if (realtyType == null)
            {
                return HttpNotFound();
            }
            return View(realtyType);
        }

        // GET: RealtyType/Create
        public ActionResult Create()
        {
            ViewBag.MainTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description");
            return View();
        }

        // POST: RealtyType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RealtyTypeID,Description,MainTypeID")] RealtyType realtyType)
        {
            if (ModelState.IsValid)
            {
                db.RealtyType.Add(realtyType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.MainTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", realtyType.MainTypeID);
            return View(realtyType);
        }

        // GET: RealtyType/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealtyType realtyType = db.RealtyType.Find(id);
            if (realtyType == null)
            {
                return HttpNotFound();
            }

            List<RealtyType> rts = db.RealtyType.ToList();

            for (var i=0; i<rts.Count; i++)
            {
                if (rts[i].RealtyTypeID == id)
                {
                    rts.Remove(rts[i]);
                }
            }

            ViewBag.MainTypeID = new SelectList(rts, "RealtyTypeID", "Description", realtyType.MainTypeID);
            return View(realtyType);
        }

        // POST: RealtyType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RealtyTypeID,Description,MainTypeID")] RealtyType realtyType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(realtyType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.MainTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", realtyType.MainTypeID);
            return View(realtyType);
        }

        // GET: RealtyType/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RealtyType realtyType = db.RealtyType.Find(id);
            if (realtyType == null)
            {
                return HttpNotFound();
            }
            return View(realtyType);
        }

        // POST: RealtyType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RealtyType realtyType = db.RealtyType.Find(id);

            List<RealtyType> list_realty_types = db.RealtyType.ToList();
            List<RealtyType> listApagar = new List<RealtyType>();

            for (int i = 0; i < list_realty_types.Count; i++)
            {
                if (list_realty_types[i].MainTypeID == id)
                {
                    listApagar.Add(list_realty_types[i]);
                }
            }

            Boolean flag = true;
            List<Ad> ad1 = db.Ad.Where(a => a.Realty.RealtyTypeID.Equals(id)).ToList();

            for (int i = 0; i < listApagar.Count; i++)
            {
                int id2 = listApagar[i].RealtyTypeID;
                List<Ad> ad2 = db.Ad.Where(a => a.Realty.RealtyTypeID.Equals(id2)).ToList();
                if (ad2.Count > 0)
                {
                    flag = false;
                    i = listApagar.Count;
                }
            }

            if (ad1.Count > 0 || !flag)
            {
                TempData["erro"] = "Impossible to delete. There are Ads of this Realty Type.";
            }
            else
            {

                List<Alert> list_alerts = db.Alert.ToList();

                listApagar.Add(realtyType);

                for (int i = 0; i < list_alerts.Count; i++)
                {
                    for (int j = 0; j < listApagar.Count; j++)
                    {

                        if (list_alerts[i].RealtyTypeID == listApagar[j].RealtyTypeID)
                        {
                            db.Alert.Remove(list_alerts[i]);
                        }
                    }
                }

                for (int j = 0; j < listApagar.Count; j++)
                {
                    db.RealtyType.Remove(listApagar[j]);
                }

                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
