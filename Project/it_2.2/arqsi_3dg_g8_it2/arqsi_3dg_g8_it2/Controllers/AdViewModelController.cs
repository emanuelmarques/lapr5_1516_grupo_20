﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RealEstateLibrary.Models;
using Microsoft.AspNet.Identity;

namespace arqsi_3dg_g8_it2.Controllers
{
    public class AdViewModelController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AdViewModel
        public ActionResult Index()
        {
            var ad = db.Ad.Include(a => a.AdType).Include(a => a.Realty.RealtyType).Include(a => a.Photos);
            return View(ad.ToList());
        }

        // GET: AdViewModel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ad ad = db.Ad.Find(id);
            if (ad == null)
            {
                return HttpNotFound();
            }
            return View(ad);
        }

        // GET: AdViewModel/Create
        public ActionResult Create()
        {
            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description");
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description");

            if (db.RealtyType.Count() == 0)
            {
                TempData["erro2"] = "Impossible to create. No Realty Type created.";

                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        // POST: AdViewModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AdViewModelID,AdTypeID,Cost,User,Address,Area,RealtyTypeID,Latitude,Longitude,Altitude,Photo")] AdViewModel adViewModel)
        {
            if (ModelState.IsValid)
            {
                adViewModel.User = User.Identity.Name;
                List<Photo> list = new List<Photo>();

                GPSLocation location = new GPSLocation() { Latitude = adViewModel.Latitude, Altitude = adViewModel.Altitude, Longitude = adViewModel.Longitude };
                Realty realty = new Realty() { Address = adViewModel.Address, Area = adViewModel.Area, GPSLocation = location, RealtyTypeID = adViewModel.RealtyTypeID , RealtyType=db.RealtyType.Find(adViewModel.RealtyTypeID)};

                db.GPSLocation.Add(location);
                db.Realty.Add(realty);

                db.SaveChanges();

                Ad ad = new Ad() { AdTypeID = adViewModel.AdTypeID, AdType=db.AdType.Find(adViewModel.AdTypeID), Cost = adViewModel.Cost, User = adViewModel.User, Photos=list, RealtyID=realty.RealtyID , Realty=db.Realty.Find(realty.RealtyID)};

                for (int i =0; i < adViewModel.Photo.Count; i++)
                {
                    if (adViewModel.Photo[i] != "")
                    {
                        Photo photo = new Photo() { URL = adViewModel.Photo[i] };
                        ad.Photos.Add(photo);
                    }
                }
               
                db.Ad.Add(ad);

                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description", adViewModel.AdTypeID);
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", adViewModel.RealtyTypeID);
            return View(adViewModel);
        }

        // GET: AdViewModel/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ad ad = db.Ad.Find(id);
            if (ad == null)
            {
                return HttpNotFound();
            }
            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description", ad.AdTypeID);
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", ad.Realty.RealtyTypeID);
            return View(ad);
        }

        // POST: AdViewModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Ad ad)
        {
            if (ModelState.IsValid)
            {

                for (int i=0; i<ad.Photos.Count; i++)
                {
                    db.Entry(ad.Photos[i]).State = EntityState.Modified;
                }

                db.Entry(ad.Realty.GPSLocation).State = EntityState.Modified;
                db.Entry(ad.Realty).State = EntityState.Modified;
                db.Entry(ad).State = EntityState.Modified;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AdTypeID = new SelectList(db.AdType, "AdTypeID", "Description", ad.AdTypeID);
            ViewBag.RealtyTypeID = new SelectList(db.RealtyType, "RealtyTypeID", "Description", ad.Realty.RealtyTypeID);
            return View(ad);
        }

        // GET: AdViewModel/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ad ad = db.Ad.Find(id);
            if (ad == null)
            {
                return HttpNotFound();
            }
            return View(ad);
        }

        // POST: AdViewModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ad ad = db.Ad.Find(id);

            for (int i=0; i < ad.Photos.Count; i++)
            {
                db.Photo.Remove(ad.Photos[i]);
                i--; 
            }

            db.GPSLocation.Remove(ad.Realty.GPSLocation);

            db.Ad.Remove(ad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
