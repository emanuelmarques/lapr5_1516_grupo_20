﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateLibrary.Models
{
   public class Notification
    {
        [Key]
        public int NotificationID { get; set; }

        public string menssage { get; set; }

        public string userName { get; set; }

        public Boolean notified { get; set; }

    }
}
