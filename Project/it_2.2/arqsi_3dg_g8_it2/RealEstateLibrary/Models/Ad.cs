﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class Ad
    {
        [Key]
        public int AdID { get; set; }

        [ForeignKey("Realty")]
        public int RealtyID { get; set; }
        public virtual Realty Realty { get; set; }

        [ForeignKey("AdType")]
        public int AdTypeID { get; set; }
        public virtual AdType AdType { get; set; }

        public decimal Cost { get; set; }

        public virtual List<Photo> Photos { get; set; } = new List<Photo>();

        public string User { get; set; }

        public string Mediator { get; set; }
    }
}