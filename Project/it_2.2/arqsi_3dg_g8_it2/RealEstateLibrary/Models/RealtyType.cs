﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class RealtyType
    {
        [Key]
        public int RealtyTypeID { get; set; }

        [DisplayName("RealtyType")]
        public string Description { get; set; }

        [ForeignKey("MainType")]
        public int? MainTypeID { get; set; }
        public virtual RealtyType MainType { get; set; }
    }
}