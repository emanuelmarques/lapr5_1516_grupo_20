﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class Realty
    {
        [Key]
        public int RealtyID { get; set; }

        [ForeignKey("RealtyType")]
        public int RealtyTypeID { get; set; }
        public virtual RealtyType RealtyType { get; set; }

        public string Address { get; set; }
        
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/MM/yy}")]
        public DateTime Day { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{hh\\:mm}")]
        public DateTime StartHour { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{hh\\:mm}")]
        public DateTime EndHour { get; set; }

        [ForeignKey("GPSLocation")]
        public int GPSLocationID { get; set; }
        public virtual GPSLocation GPSLocation { get; set; }

        public decimal Area { get; set; }

        public virtual List<Visit> Schedule { get; set; } = new List<Visit>();

    }
}