﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class Photo
    {
        [Key]
        public int PhotoID { get; set; }
        public string URL { get; set; }
    }
}