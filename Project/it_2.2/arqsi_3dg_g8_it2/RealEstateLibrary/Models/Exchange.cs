﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class Exchange : AdType
    {
        [Key]
        public int ExchangeID { get; set; }
    }
}