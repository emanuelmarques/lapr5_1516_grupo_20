﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace RealEstateLibrary.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        public virtual List<Visit> Schedule { get; set; } = new List<Visit>();

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<Ad> Ad { get; set; }
        public DbSet<AdType> AdType { get; set; }
        public DbSet<RealtyType> RealtyType { get; set; }
        public DbSet<Realty> Realty { get; set; }
        public DbSet<GPSLocation> GPSLocation { get; set; }
        public DbSet<Alert> Alert { get; set; }
        public DbSet<Photo> Photo { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Availability> Availability { get; set; }
        public DbSet<Notification> Notification { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

    }
}