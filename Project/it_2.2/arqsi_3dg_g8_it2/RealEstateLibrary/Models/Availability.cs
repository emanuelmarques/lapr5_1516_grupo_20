﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateLibrary.Models
{
    public class Availability
    {
        [Key]
        public int AvaibilityID { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yy}")]
        public DateTime Day { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{hh\\:mm}")]
        public DateTime StartHour { get; set; }

        [Required]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{hh\\:mm}")]
        public DateTime EndHour { get; set; }

        public string UserName { get; set; }

        [ForeignKey("Realty")]
        public int RealtyID { get; set; }
        public virtual Realty Realty { get; set; }



    }
}
