﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public abstract class AdType
    {
        [Key]
        public int AdTypeID { get; set; }
        [DisplayName("AdType")]
        public string Description { get; set; }

    }
}