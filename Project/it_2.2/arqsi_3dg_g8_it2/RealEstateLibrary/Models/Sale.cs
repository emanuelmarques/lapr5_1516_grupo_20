﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class Sale : AdType
    {
        [Key]
        public int SaleID { get; set; }
    }
}