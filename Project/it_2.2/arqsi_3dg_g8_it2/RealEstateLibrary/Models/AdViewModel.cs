﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateLibrary.Models
{
    public class AdViewModel
    {
        [ForeignKey("AdType")]
        public int AdTypeID { get; set; }
        public virtual AdType AdType { get; set; }
        public decimal Cost { get; set; }
        public string User { get; set; }

        public string Address { get; set; }
        public decimal Area { get; set; }
        [ForeignKey("RealtyType")]
        public int RealtyTypeID { get; set; }
        public virtual RealtyType RealtyType { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{dd/MM/yy}")]
        public DateTime Day { get; set; }

        
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{hh\\:mm}")]
        public DateTime StartHour { get; set; }

        
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{hh\\:mm}")]
        public DateTime EndHour { get; set; }

        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public decimal Altitude { get; set; }

        public List<string> Photo { get; set; } = new List<string>();

    }
}