namespace RealEstateLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_visit_mediador1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Visits", "Mediador");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Visits", "Mediador", c => c.String());
        }
    }
}
