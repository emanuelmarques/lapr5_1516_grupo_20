namespace RealEstateLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ads", "Mediator", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ads", "Mediator");
        }
    }
}
