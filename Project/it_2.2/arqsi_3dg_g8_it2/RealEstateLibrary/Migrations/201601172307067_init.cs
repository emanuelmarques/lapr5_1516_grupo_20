namespace RealEstateLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Visits", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Visits", new[] { "UserID" });
            AddColumn("dbo.Visits", "ApplicationUser_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.Visits", "UserID", c => c.String());
            CreateIndex("dbo.Visits", "ApplicationUser_Id");
            AddForeignKey("dbo.Visits", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Visits", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Visits", new[] { "ApplicationUser_Id" });
            AlterColumn("dbo.Visits", "UserID", c => c.String(maxLength: 128));
            DropColumn("dbo.Visits", "ApplicationUser_Id");
            CreateIndex("dbo.Visits", "UserID");
            AddForeignKey("dbo.Visits", "UserID", "dbo.AspNetUsers", "Id");
        }
    }
}
