namespace RealEstateLibrary.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_visit_mediador : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Visits", "Mediador", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Visits", "Mediador");
        }
    }
}
