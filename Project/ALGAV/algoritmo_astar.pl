menor_percursoh([X],X):-!.
menor_percursoh([c(F/_,_)|L],c(FM/GM,P)):-Menor=c(FM/GM,P),menor_percursoh(L,Menor),FM<F,!.
menor_percursoh([X|_],X).

hbf(Orig,Dest,Perc,Total):-
			estimativa(Orig,Dest,H), F is H + 0, % G = 0.
			hbf1([c(F/0,[Orig])],Dest,P,Total),
			reverse(P,Perc).
%select(?Elem, ?List1, ?List2).
hbf1(Percursos,Dest,Percurso,Total):-
	                %menor_percursoh(Percursos,Menor),Restantes),
                    menor_percursoh(Percursos,Menor),
			select(Menor,Percursos,Restantes),
			percursos_seguintesh(Menor,Dest,Restantes,Percurso,Total).

percursos_seguintesh(c(_/Dist,Percurso),Dest,_,Percurso,Dist):-Percurso=[Dest|_],!.
%verifica se o primeiro elemento do percurso ? o destino.
%percursos_seguintesh(c(_,[Dest|_]),Dest,Restantes,Percurso,Total):-!,
%	hbf1(Restantes,Dest,Percurso,Total).
%procura outras solu??es se encontrou o caminho.
percursos_seguintesh(c(_/Dist,[Ult|T]),Dest,Percursos,Percurso,Total):-

	findall(c(F1/D1,[Z,Ult|T]),proximo_noh(Ult,T,Z,Dist,Dest,F1/D1),Lista),
	%junta z ao ult que foi visitado, passa a proximo n? F1/D1, Dist ?
	append(Lista,Percursos,NovosPercursos),
	hbf1(NovosPercursos,Dest,Percurso,Total).



proximo_noh(X,T,Y,Dist,Dest,F/Dist1):-
					(estrada(X,Y,Z);estrada(Y,X,Z)),
					\+ member(Y,T),
					Dist1 is Dist + Z,
					estimativa(Y,Dest,H), F is H + Dist1.

estimativa(C1,C2,Est):-
		localizacao(C1,X1,Y1),
		localizacao(C2,X2,Y2),
		DX is X1-X2,
		DY is Y1-Y2,
		Est is sqrt(DX*DX+DY*DY).

% estimativa(_,_,0). % para desprezar a heur?stica.