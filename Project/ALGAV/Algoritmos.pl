:- use_module(library(http/json_convert)).
:- use_module(library(error)).

%Compara�ao de dois tempos, se as horas forem iguais compara os
% minutos, se nao forem, compara as horas.

earlier(H:M1, H:M2) :- !, M1 < M2.
earlier(H1:_, H2:_) :- H1 < H2.

%HIM-hora de inicio do mediador
%HFM-hora de fim do mediador
%HIC-hora de inicio do cliente
%HFC-hora de fim do cliente
% 4 possibilidades de interseca�ao entre os intervalos de tempo da
% disponibilidade
compatibilidade_horas(HIM,HFM,HIC,HFC,HIM,HFC):-earlier(HIC,HIM),earlier(HFC,HFM),earlier(HIM,HFC).
compatibilidade_horas(HIM,HFM,HIC,HFC,HIC,HFC):-earlier(HIM,HIC),earlier(HFC,HFM).
compatibilidade_horas(HIM,HFM,HIC,HFC,HIC,HFM):-earlier(HIM,HIC),earlier(HIC,HFM),earlier(HFM,HFC).
compatibilidade_horas(HIM,HFM,HIC,HFC,HIM,HFM):-earlier(HIC,HIM),earlier(HFM,HFC).

%Quando as horas de inicio sao iguais.
compatibilidade_horas(HIM,HFM,HIM,HFC,HIM,HFC):-earlier(HFC,HFM).
compatibilidade_horas(HIM,HFM,HIM,HFC,HIM,HFM):-earlier(HFM,HFC).

%Quando as horas de fim sao iguais.
compatibilidade_horas(HIM,HFM,HIC,HFM,HIC,HFM):-earlier(HIM,HIC).
compatibilidade_horas(HIM,HFM,HIC,HFM,HIM,HFM):-earlier(HIC,HIM).

%Quando as horas de fim e in�cio sao iguais.
compatibilidade_horas(HIM,HFM,HIM,HFM,HIM,HFM).

% match, correspondencia entre a disponibilidade do mediador e a
% disponibilidade do cliente.
% match(IDMEDIADOR,IDCLIENTE,HORAINICIO,HORAFIM)

%IDM -ID do Mediador
%IDC -ID do Cliente
%HIM -Hora de In�cio do Mediador
%HFM -Hora de Fim do Mediador
%HIC -Hora de In�cio do Cliente
%HFC -Hora de Fim do Cliente
%

:-consult('BaseDeConhecimento').
% mediador(100,[disponibilidade(date(2015-10-10),time(16:00),time(18:10))]).
% cliente_imovel(1,2,[disponibilidade(date(2015-10-10),time(17:00),time(18:10))])
%imovel(2,disponibilidade(date(2015-10-10),time(17:30),time(18:10))).

criar_match(IDM,IDC,IDI,disponibilidade(DIA,HIM,HFM),disponibilidade(D,HIC,HFC)):-
	DIA==D,
	compatibilidade_horas(HIM,HFM,HIC,HFC,HIF,HFF),
        assertz(match(IDM,IDC,IDI,DIA,HIF,HFF)).


iterar_disponibilidades(_,[],_).
iterar_disponibilidades(IDM,[H|T],L):-
	iterar_cliente_imovel(IDM,H,L),iterar_disponibilidades(IDM,T,L).

iterar_disp_cliente_imovel(_,_,_,_,[]).
iterar_disp_cliente_imovel(IDM,IDC,IDI,DM,[H|T]):-%imovel(IDI,Disp),
	                                          criar_match(IDM,IDC,IDI,DM,H)%,Disp)
						  ,iterar_disp_cliente_imovel(IDM,IDC,IDI,DM,T).

iterar_cliente_imovel(_,_,[]).
iterar_cliente_imovel(IDM,DM,[(IDC,IDI,L)|T]):-iterar_disp_cliente_imovel(IDM,IDC,IDI,DM,L),iterar_cliente_imovel(IDM,DM,T).

criar_visita(IDM):-mediador(IDM,LD),
	retractall(match(_,_,_,_,_)),
	           findall((IDC,IDI,L),cliente_imovel(IDC,IDI,L),NL),
		   iterar_disponibilidades(IDM,LD,NL),fail;true.

:-json_object
        match(username_mediador:string,username_cliente:string,id_imovel:integer,dia:string, hora_inicio:string,hora_fim:string).

matches_to_json(NL):-findall((IDM,IDC,IDI,DIA,HI,HF),match(IDM,IDC,IDI,DIA,HI,HF),L),iterar_lista_matches(L,NL).

%2016-01-16T16:50:33.7124228+00:00
iterar_lista_matches([],[]).
iterar_lista_matches([(IDM,IDC,IDI,DIA,HI,HF)|T],[M|NL]):-
							  %atom_number(IDM,A),
							  %atom_number(IDC,B),
							  %atom_number(IDI,C),
	                                                  term_to_atom(IDM,IDM_A),
							  term_to_atom(IDC,IDC_A),
							  term_to_atom(DIA,DIA_A),
							  term_to_atom(HI,HI_A),
							  term_to_atom(HF,HF_A),


							  atomics_to_string([IDM_A],"",UMEDIADORM),
							  atomics_to_string([IDC_A],"",UCLIENTEM),
							  atomics_to_string([DIA_A,'T00:00:00.805439+00:00'],"",DiaM),
							  atomics_to_string([DIA_A,'T',HI_A,':00.805439+00:00'],"",HoraInicioM),
							  atomics_to_string([DIA_A,'T',HF_A,':00.805439+00:00'],"",HoraFimM),
							  %atom_codes(A,D),
							  %atom_codes(B,E),
							  %atom_codes(C,F),
	                                                  prolog_to_json(match(UMEDIADORM,UCLIENTEM,IDI,DiaM,HoraInicioM,HoraFimM),M),
							  iterar_lista_matches(T,NL).

carregar():-assertz(cliente('josefina')),
	    assertz(mediador('joaquim',[disponibilidade(2015-10-10,17:30,18:40)])),
	    assertz(cliente_imovel('joaquim',100,[disponibilidade(2015-10-10,17:30,18:40)])).
