<?php
namespace Mediador\Form;
use Zend\Form\Form;

class AdVMForm extends Form
{
    
    
    public function __construct($name = null)
    {
      
    
        $this->add(array(
            'status' => 'status',
            'type' => 'boolean',
            'options' => array(
                'label' => 'status',
            ),
        ));
        $this->add(array(
            'name' => 'AdID',
            'type' => 'integer',
            'options' => array(
                'label' => 'Password',
            ),
        ));
        
        $this->add(array(
            'name' => 'RealtyID',
            'type' => 'integer',
            'options' => array(
                'label' => 'RealtyID',
            ),
        ));
        
        $this->add(array(
            'name' => 'RealtyTypeID',
            'type' => 'integer',
            'options' => array(
                'label' => 'RealtyTypeID',
            ),
        ));
        
        $this->add(array(
            'name' => 'RealtyTypeDescription',
            'type' => 'String',
            'options' => array(
                'label' => 'RealtyTypeDescription',
            ),
        ));
        

        $this->add(array(
            'name' => 'AdTypeID',
            'type' => 'integer',
            'options' => array(
                'label' => 'AdTypeID',
            ),
        ));
        
        $this->add(array(
            'name' => 'AdTypeDescription',
            'type' => 'String',
            'options' => array(
                'label' => 'AdTypeDescription',
            ),
        ));
       
        $this->add(array(
            'name' => 'Address',
            'type' => 'String',
            'options' => array(
                'label' => 'Address',
            ),
        ));
        
        

        $this->add(array(
            'name' => 'Area',
            'type' => 'integer',
            'options' => array(
                'label' => 'Area',
            ),
        ));
         
        $this->add(array(
            'name' => 'GPSLocationID',
            'type' => 'integer',
            'options' => array(
                'label' => 'GPSLocationID',
            ),
        ));
         
        $this->add(array(
            'name' => 'GPSLocationID',
            'type' => 'integer',
            'options' => array(
                'label' => 'GPSLocationID',
            ),
        ));
        
        $this->add(array(
            'name' => 'Latitude',
            'type' => 'integer',
            'options' => array(
                'label' => 'Latitude',
            ),
        ));
        
        $this->add(array(
            'name' => 'Longitude',
            'type' => 'integer',
            'options' => array(
                'label' => 'Longitude',
            ),
        ));
        
        $this->add(array(
            'name' => 'Altitude',
            'type' => 'integer',
            'options' => array(
                'label' => 'Altitude',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'Cost',
            'type' => 'integer',
            'options' => array(
                'label' => 'Cost',
            ),
        ));
        
        $this->add(array(
            'name' => 'User',
            'type' => 'integer',
            'options' => array(
                'label' => 'User',
            ),
        ));
             
    
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));
    }
}
    


?>