<?php
namespace Mediador\Services;


use Zend\Http\Client;
use Zend\Http\Request;
use Zend\Json\Json;
use Mediador\DTO\Credenciais;

class ImoServices
{
    const URL_Login = 'http://localhost:50371/Token';
    //const URL_Values = 'https://10.0.1.4/WebApplication8/api/values';
    
    public static function Login(Credenciais $credenciais)
    {
        $client = new Client(self::URL_Login);
        
        

        $client->setMethod(Request::METHOD_POST);
        $params = 'grant_type=password&username=' . $credenciais->username .'&password=' .$credenciais->password;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len,
            'Content-Enconde'     =>"UTF-8"
          
        ));        
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);

        $response = $client->send();    
      
        
        $body=Json_decode($response->getBody());
      
        if(!empty($body->access_token))
        {
            session_start();

            $_SESSION['access_token'] = $body->access_token;
            $_SESSION['username'] = $credenciais->username;

            return true;
        }
        else 
            return false;
    }
    
    public static function Logout()
    {
       // session_start();
        
        $_SESSION['username'] = null;
        $_SESSION['access_token'] = null;
    }
    
    public static function getValues()
    {
        $client = new Client(self::URL_Values);
        
        $client->setMethod(Request::METHOD_GET);
        
        session_start();
        
        $bearer_token = 'Bearer ' . $_SESSION['access_token'];
        
        $client->setHeaders(array(
            'Authorization'   => $bearer_token,
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        
        $response = $client->send();
        
        $body=$response->getBody();
        
        return $body;
    }
}


?>