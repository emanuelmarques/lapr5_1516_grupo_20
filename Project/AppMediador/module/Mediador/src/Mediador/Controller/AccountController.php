<?php
namespace Mediador\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Mediador\Form\LoginForm;
use Mediador\Services\ImoServices;
use Mediador\DTO\Credenciais;


/**
 * AccountController
 *
 * @author
 *
 * @version
 *
 */
class AccountController extends AbstractActionController
{

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
        // TODO Auto-generated AccountController::indexAction() default action
        return new ViewModel();
    }
    
    public function loginAction()
    {
        session_start();
        $form = new LoginForm();
        $form->get('submit')->setValue('Login');
    
        $request = $this->getRequest();
        if ($request->isPost()) {
    
            
          
            ImoServices::Logout();
            $_SESSION['username'] = $_POST['username']; // store username
            $_SESSION['password'] = $_POST['password']; // store password
            $credenciais = new Credenciais();
            $form->setInputFilter($credenciais->getInputFilter());
            $form->setData($request->getPost());
            
            
    
            if ($form->isValid()) {
                $credenciais->exchangeArray($form->getData());
                 
                if( ImoServices::Login($credenciais) )
    
                    // Redirect to values
                    return $this->redirect()->toRoute('Ad', array('controller'=>'Ad', 'action' => 'index'));
            }
        }
        return array('form' => $form);
    }
    
    public function logoutAction()
    {
        ImoServices::Logout();
    
        return $this->redirect()->toRoute('Mediador', array('controller'=>'account', 'action' => 'login'));
    }
    
    public function valuesAction()
    {
        $body = ImoServices::getValues();
    
        return new ViewModel(array(
            'values' => Json::decode($body)
        ));
    }
    
    
}