<?php
namespace Mediador\Controller;
use Zend\Http\Client;
use Zend\Http\Request;
//use Mediador\AdForm;
use Mediador\Model\Ad;
use Mediador\ViewModel\AdVM;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\View;

/**
 * AdController
 *
 * @author
 *
 * @version
 *
 */
class AdController extends AbstractActionController
{

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
        $client = new Client();
        $client->setUri('http://localhost:50371/api/Ad');
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30
        ));
        $response = $client->send();
    
        ///////////////// ZEND METHODS ////////////
        
        //http://framework.zend.com/manual/1.12/en/zend.http.response.html
        
        //*********DIRETO *************************/
        
        $view = new ViewModel();
        $view->anuncios = json_decode($response->getBody(), true);
        
        
        //************ POR MODEL **************//          
        $listAd[]= array();
       
       // $view = new ViewModel();
        $anuncios = json_decode($response->getBody(), true);
        $ad = new AdVM();
        foreach ($anuncios as $anuncio){
            $ad->build($anuncio);
          $listAd['Array']= $ad;
        }
        
      
              
        // TODO Auto-generated AdController::indexAction() default action
        return $view;
    }

    public function addAction()
    {
       
        return new ViewModel();
     
    }
   
    public function mediateAction(){
        $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Ad/'.$id;

        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30,
            'Content-lenght' =>900,
        ));
        
        $response = $client->send();
        $response_array = json_decode($response->getBody(), true);
        session_start(); 
        $response_array['Mediator']= $_SESSION['username'];
        
    
        

        $send = json_encode($response_array);
        var_dump($send);
      
        $ch = curl_init('http://localhost:50371/api/Ad/'.$id);                                                                      
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $send);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($send))                                                                       
        );                                                                                                                   
                                                                                                                     
        $result = curl_exec($ch);
       
 
      
        return $this->redirect()->toRoute('Ad', array('controller'=>'Ad', 'action' => 'index'));
        
    }

    public function editAction()
    { $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Ad/'.$id;
        //var_dump($pedido);
        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30,
            'Content-lenght' =>900,
        ));
        
        $response = $client->send();
        //var_dump($response->getBody());
        $view = new ViewModel();
        //$edit;
        $view->edit = json_decode($response->getBody(), true);
       // var_dump($edit);
       
        return $view;
    }
    public function availabilityAction()
    { 
        
        $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Ad/'.$id;
        //var_dump($pedido);
        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30,
            'Content-lenght' =>900,
        ));
        
        $response = $client->send();
        //var_dump($response->getBody());
        $view = new ViewModel();
        //$edit;
        $view->edit = json_decode($response->getBody(), true);

    //$edit;
   // $view->edit = json_decode($response->getBody(), true);
    // var_dump($edit);
     
    return $view;
    }

        public function SaveAction()
        {
            
             $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Ad/'.$id;
        echo "BLADJSADA";
        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30,
            'Content-lenght' =>900,
        ));
        
        $response = $client->send();
        $response_array = json_decode($response->getBody(), true);
        
        $response_array['Status'] = $_GET["Status"];
        $response_array['AdType'] = $_GET["AdType"];
        $response_array['Realty'] = $_GET["Realty"];
        $response_array['Address'] = $_GET["Address"];
        $response_array['Area'] = $_GET["Area"];
        $response_array['MainType'] = $_GET["MainType"];
        $response_array['Latitude'] = $_GET["Latitude"];
        $response_array['Longitude'] = $_GET["Longitude"];
        $response_array['Altitude'] = $_GET["Altitude"];
        
        
       // var_dump($response_array);
        
        $send = json_encode($response_array);
         
         
         $ch = curl_init('http://localhost:50371/api/Ad/'.$id);
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
         curl_setopt($ch, CURLOPT_POSTFIELDS, $send);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_HTTPHEADER, array(
             'Content-Type: application/json',
             'Content-Length: ' . strlen($send))
             );
        
        $result = curl_exec($ch);
     
         
     
         return $this->redirect()->toRoute('Ad', array('controller'=>'Ad', 'action' => 'index'));
     
        }
        
    
    public function deleteAction()
    {
      
        $id= (int) $this->params()->fromRoute('id', 0);
        
       $url = 'http://localhost:50371/api/Ad/'.$id;
        
        $client = new Client($url);
        
        $client->setMethod(Request::METHOD_DELETE);
        $params = 'grant_type=int&Name=' .$id ;
        
        $len = strlen($params);
        
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
        
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
   //    var_dump($response);
        $response = $client->send();
       
        $body=Json_decode($response->getBody(), true);
      
    
        return $this->redirect()->toRoute('Ad', array('controller'=>'Ad', 'action' => 'index'));
       
    
    }
    
    

    public function detailsAction()
    {
        $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Ad/'.$id;
        //var_dump($pedido);
        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30
        ));
        
        $response = $client->send();
        
        //var_dump($response);
        $view = new ViewModel();
        $view->details = json_decode($response->getBody(), true);
        
       
        return $view;
      }
    
    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /ad/ad/foo
        return array();
    }
}