<?php
namespace Mediador\Controller;
use Zend\Http\Request;
use Zend\Http\Client;
//use Mediador\AdForm;
use Mediador\Model\Ad;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Mediador\ViewModel\AvailabilityVM;
use Zend\Db\Sql\Ddl\Column\Boolean;
use Zend\View\View;

/**
 * AdController
 *
 * @author
 *
 * @version
 *
 */
class VisitController extends AbstractActionController
{

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
        $client = new Client();
        $client->setUri('http://localhost:50371/api/Visit');
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30
        ));
        $response = $client->send();
    
        ///////////////// ZEND METHODS ////////////
        
        //http://framework.zend.com/manual/1.12/en/zend.http.response.html
        
        //*********DIRETO *************************/
        
     $view = new ViewModel();
        $view->visitas = json_decode($response->getBody(), true);
        
        
        //************ POR MODEL **************//          
        $listAd[]= array();
       
       // $view = new ViewModel();
        $visitas = json_decode($response->getBody(), true);
       
        $visit = new AvailabilityVM();
        session_start();
      
        $i = 0;
        foreach ($visitas as $visita){
            
            if ($visita['User']==$_SESSION['username']){
                $visit->build($visita);
                $listAd[$i]= $visit;
                $i++;
            }
        }
        
      
              
        // TODO Auto-generated AdController::indexAction() default action
        return $view;
    }
    
    public function generateAction()
    { 
    $view = new ViewModel();
    //$edit;
    
   
     
    return $view;
    }

 
   
    

    
   
    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /ad/ad/foo
        return array();
    }
}