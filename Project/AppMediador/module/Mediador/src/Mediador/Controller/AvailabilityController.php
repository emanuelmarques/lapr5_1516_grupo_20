<?php
namespace Mediador\Controller;
use Zend\Http\Request;
use Zend\Http\Client;
//use Mediador\AdForm;
use Mediador\Model\Ad;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Mediador\ViewModel\AvailabilityVM;
use Zend\Db\Sql\Ddl\Column\Boolean;
use Zend\View\View;

/**
 * AdController
 *
 * @author
 *
 * @version
 *
 */
class AvailabilityController extends AbstractActionController
{

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {
        $client = new Client();
        $client->setUri('http://localhost:50371/api/Availability');
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30
        ));
        $response = $client->send();
    
        ///////////////// ZEND METHODS ////////////
        
        //http://framework.zend.com/manual/1.12/en/zend.http.response.html
        
        //*********DIRETO *************************/
        
        $view = new ViewModel();
        $view->visitas = json_decode($response->getBody(), true);
        
        
        //************ POR MODEL **************//          
        $listAd[]= array();
       
       // $view = new ViewModel();
        $visitas = json_decode($response->getBody(), true);
       
        $visit = new AvailabilityVM();
        session_start();
        foreach ($visitas as $visita){
            if ($visita['User']==$_SESSION['username']){
                $visit->build($visita);
                $listAd['Array']= $visit;
            }
        }
        
      
              
        // TODO Auto-generated AdController::indexAction() default action
        return $view;
    }


    public function editAction()
        { $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Availability/'.$id;
        //var_dump($pedido);
        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30,
            'Content-lenght' =>900,
        ));
        
        $response = $client->send();
        //var_dump($response->getBody());
        $view = new ViewModel();
        //$edit;
        $view->edit = json_decode($response->getBody(), true);
        // var_dump($edit);
         
        return $view;
    }
    
   
   
    
    
    
    public function deleteAction()
    {
    
        $id= (int) $this->params()->fromRoute('id', 0);
    
        $url = 'http://localhost:50371/api/Availability/'.$id;
    
        $client = new Client($url);
    
        $client->setMethod(Request::METHOD_DELETE);
        $params = 'grant_type=int&Name=' .$id ;
    
        $len = strlen($params);
    
        $client->setHeaders(array(
            'Content-Type'   => 'application/x-www-form-urlencoded',
            'Content-Length' => $len
        ));
    
        $client->setOptions(['sslverifypeer' => false]);
        $client->setRawBody($params);
        //    var_dump($response);
        $response = $client->send();
         
        $body=Json_decode($response->getBody(), true);
    
    
        return $this->redirect()->toRoute('Availability', array('controller'=>'Availability', 'action' => 'index'));
         
    
    }
    public function detailsAction()
    {
        $id= (int) $this->params()->fromRoute('id', 0);
        $client = new Client();
        $pedido= 'http://localhost:50371/api/Availability/'.$id;
        //var_dump($pedido);
        $client->setUri($pedido);
        $client->setOptions(array(
            'maxredirects' => 0,
            'timeout'      => 30
        ));
    
        $response = $client->send();
    
        //var_dump($response);
        $view = new ViewModel();
        $view->details = json_decode($response->getBody(), true);
    
         
        return $view;
    }
    
    

    
   
    public function fooAction()
    {
        // This shows the :controller and :action parameters in default route
        // are working when you browse to /ad/ad/foo
        return array();
    }
}