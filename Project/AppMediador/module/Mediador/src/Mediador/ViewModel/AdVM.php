<?php
namespace Mediador\ViewModel;

class AdVM
{
    public $status;
    
    public $AdID;
    
    public $RealtyID;
    
    public $RealtyTypeID;
    
    public $RealtyTypeDescription;
    
    public $AdTypeID;
    
    public $AdTypeDescription;
    
    public $Address;
    
    public $Area;
    
    public $GPSLocationID;
    
    public $Latitude;
    
    public $Longitude;
    
    public $Altitude;
    
    public $Cost;
    
    public $Photos;
    
    public $User;
    
    public $Mediator;
    
    
    public function build($ad){
      // var_dump($ad);
       $this->AdID = $ad['AdID'];
       $this->RealtyID = $ad['RealtyID'];
       $this->RealtyTypeID = $ad['RealtyTypeID'];
       $this->RealtyTypeID = $ad['RealtyTypeID'];
       $this->RealtyTypeDescription = $ad['RealtyTypeDescription'];
       $this->AdTypeID = $ad['AdTypeID'];
       $this->AdTypeDescription = $ad['AdTypeDescription'];
       $this->Address = $ad['Address'];
       $this->Area = $ad['Area'];
       $this->GPSLocationID = $ad['GPSLocationID'];
       $this->Latitude = $ad['Latitude'];
       $this->Longitude = $ad['Longitude'];
       $this->Altitude = $ad['Altitude'];
       $this->Cost = $ad['Cost'];
       $this->User = $ad['User'];
       $this->Mediator = $ad['Mediator'];
   

   
    }
    

    
    public function getAd()
   {
       return $this;
   }
    
}

?>