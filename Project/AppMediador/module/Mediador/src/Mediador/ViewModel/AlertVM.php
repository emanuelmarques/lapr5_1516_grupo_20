<?php
namespace Mediador\ViewModel;

class AlertVM
{
    public $AlertID;
    
    public $AdTypeID;
    
    public $AdTypeDescription;
    
    public $RealtyTypeID;
    
    public $RealtyTypeDescription;
    
    public $Address;
    
    public $MaxCost;
    
    public $MaxArea;
    
    public $UserName;
}

?>