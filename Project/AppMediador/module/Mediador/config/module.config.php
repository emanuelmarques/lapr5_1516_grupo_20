<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Mediador\Controller\Availability'=>'Mediador\Controller\AvailabilityController',
            'Mediador\Controller\Visit'=>'Mediador\Controller\VisitController',
            'Mediador\Controller\Ad' => 'Mediador\Controller\AdController',
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Mediador\Controller\Account' => 'Mediador\Controller\AccountController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Literal',
                'options' => array(
                    // Change this to something specific to your module
                    'route' => '/',
                    'defaults' => array(
                        // Change this value to reflect the namespace in which
                        // the controllers for your module are found
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'index',
                        'action' => 'index'
                    )
                )
            )
            ,
    
            'Ad' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/ad',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Mediador\Controller',
                        'controller' => 'ad',
                        'action' => 'index'
    
                    )
                ),
                   'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:action][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array()
                        )
                    )
                )
                ),
            
            'Account' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/Account[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Mediador\Controller\Account',
                        'action'     => 'login',
                    ),
                ),
            ),
            'Visit' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/Visit[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Mediador\Controller\Visit',
                        'action'     => 'index',
                    ),
                ),
            ),
            
           'Availability' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/Availability',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Mediador\Controller',
                        'controller' => 'Availability',
                        'action' => 'index'
    
                    )
                ),
                   'may_terminate' => true,
                'child_routes' => array(
                    // This route is a sane default when developing a module;
                    // as you solidify the routes for your module, however,
                    // you may want to remove it and replace it with more
                    // specific routes.
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:action][/:id]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ),
                            'defaults' => array()
                        )
                    )
                )
                ),
                ),
                
            ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Mediador' => __DIR__ . '/../view',
        ),
    ),
);
